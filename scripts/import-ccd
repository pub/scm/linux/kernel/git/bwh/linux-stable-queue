#!/bin/bash -eu

SCRIPTS="$(dirname "$0")"
. "$SCRIPTS"/lsq-defs

set -o pipefail

export GIT_DIR="$UPSTREAM_GIT/.git"

case $# in
    0)
	endpoint=''
	;;
    1)
	endpoint="$1"
	if git rev-list "$UPSTREAM_REMOTE/master..$endpoint" | grep -q .; then
	    echo >&2 "$endpoint is not an ancestor of $UPSTREAM_REMOTE/master"
	fi
	;;
    *)
	echo >&2 "Usage: $0 [<endpoint>]"
	exit 2
	;;
esac

if [ -f "$STABLE_QUEUES/upstream-commits" ]; then
    # Need to continue
    if [ "$endpoint" ]; then
	last_endpoint="$(cat "$STABLE_QUEUES/upstream-head")"
	if [ "$(git rev-list "$endpoint" -1)" != "$last_endpoint" ]; then
	    echo >&2 "Last import-ccd up to $last_endpoint has not been completed"
	    exit 2
	fi
    fi
    skip="$(cat "$STABLE_QUEUES/upstream-commits-done")"
else
    if [ -z "$endpoint" ]; then
	 endpoint="$UPSTREAM_REMOTE/master"
    fi

    # Iterate over all patches since last import that were cc'd to stable
    # or have a Fixes: tag
    git rev-list --no-merges -i -E --grep '^(Cc:\s*(.*<)?stable@(vger\.)?kernel\.org\b|Fixes:)' \
	"$(cat "$STABLE_QUEUES/upstream-head")".."$endpoint" | \
	tac > "$STABLE_QUEUES/upstream-commits"
    git rev-list "$endpoint" -1 >"$STABLE_QUEUES/upstream-head"
    skip=0
fi

index=0
total="$(wc -l "$STABLE_QUEUES/upstream-commits")"
total="${total% *}"

read_user_bool() {
    local prompt="$1"
    local REPLY
    while true; do
	read -p "$prompt " </dev/tty
	if [ "${REPLY#[ny]}" != "$REPLY" ]; then
	    break
	fi
    done
    test "${REPLY#n}" = "$REPLY" || return 1
}

while read commit; do
    index=$((index + 1))
    if [ $index -le $skip ]; then
	continue
    fi

    printf '[%d/%d] %s\n' $index $total "$(git log --pretty='%h %s' $commit -1)"
    read patch rest < <("$SCRIPTS"/git-format-patch-for-backport --no-signature --subject-prefix='' $commit -1)
    if [ -n "$rest" ]; then
	# Patch is commented as being for some range of stable versions.
	# Don't try to parse the comment.
	echo "Comment: $rest"
    fi
    fixes="$(sed -n 's/^Fixes: //p' "$patch" || true)"
    if [ -n "$fixes" ]; then
	echo "Fixes: $fixes"
    fi
    if git notes list $commit &>/dev/null && read_user_bool "View notes?"; then
	git notes show $commit </dev/tty
    fi
    "$SCRIPTS"/signoff-patch "$patch"
    for version in $STABLE_BASE_VERSIONS; do
	queue="$(get_queue $version)"
	if [ -n "$rest" -o -n "$fixes" ]; then
	    if ! read_user_bool "Do we want this in $version?"; then
		echo "Not adding to $version"
		continue
	    fi
	fi
	# Can't use quilt import here because we're adding to the end
	cp "$patch" $queue/
	patch2="$(rename-patch "$queue/$patch")"
	patch2="$(basename "$patch2")"
	echo "Adding to $version as $patch2"
	echo "$patch2" >> $queue/series
    done
    rm "$patch"

    echo $index > "$STABLE_QUEUES/upstream-commits-done"
done < "$STABLE_QUEUES/upstream-commits"

# Clear continuation state
rm -f "$STABLE_QUEUES/upstream-commits"
rm -f "$STABLE_QUEUES/upstream-commits-done"
