#!/bin/bash -eu

. "$(dirname "$0")"/lsq-defs

full_version="$(make kernelversion)"
base_ver="$(base_version "$full_version")"

export GIT_DIR="$PWD/.git"
export QUILT_PATCHES="$(get_queue $base_ver)"
commit="$1"

patch_orig="$(mktemp)"
patch_tmp="$(mktemp --tmpdir="$QUILT_PATCHES")"
trap 'rm -f "$patch_orig" "$patch_tmp"' EXIT

git format-patch -k "$commit" -1 --stdout > "$patch_orig"

up_commit="$(sed -n 's/^commit \(.*\) upstream\.$/\1/p;
                     s/^\[ Upstream commit \(.*\) \]$/\1/p;
                     s/^(cherry picked from commit \(.*\))$/\1/p' \
             "$patch_orig")"
if [ -z "$up_commit" ]; then
    echo >&2 "W: Upstream commit hash not found in $commit\n";
    up_commit=UNKNOWN
fi

echo "From: $MAIL_FROM" > "$patch_tmp"
echo "Date: $(date -R)" >> "$patch_tmp"
sed -n '/^Subject:/,/^\([^ 	]\|$\)/ { s/^Subject: \(.*\)/Subject: Revert "\1"/p; /^[ 	]/p }' < "$patch_orig" >> "$patch_tmp"
echo >> "$patch_tmp"
echo >> "$patch_tmp" "This reverts commit $commit, which was"
echo >> "$patch_tmp" "commit $up_commit upstream."
echo >> "$patch_tmp"
echo >> "$patch_tmp" "INSERT EXPLANATION HERE"
echo >> "$patch_tmp"
echo >> "$patch_tmp" "Signed-off-by: $MAIL_FROM"
echo >> "$patch_tmp" "---"
interdiff -p1 "$patch_orig" /dev/null | grep -v '^reverted:' >> "$patch_tmp"

patch="$(rename-patch "$patch_tmp")"
echo "Adding $patch"
basename "$patch" >> "$QUILT_PATCHES/series"
