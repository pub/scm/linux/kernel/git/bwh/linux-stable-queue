#!/usr/bin/perl

use strict;
use warnings;

use File::Basename;
use lib File::Basename::dirname($0);
use LinuxStableQueue qw($MAIL_FROM $MAIL_ANNOUNCE_TO $MAIL_ANNOUNCE_CC
			base_version last_update common_mail_header
			mime_entity_to_mbox);

use FileHandle;
use MIME::Entity;

my $FULL_VERSION = `make kernelversion`;
chomp $FULL_VERSION;
my $BASE_VERSION = base_version($FULL_VERSION);
my $LAST_VERSION = last_update($FULL_VERSION);

my $mbox = "linux-$FULL_VERSION-announce.mbox";
print "Creating $mbox\n";

my $announce = MIME::Entity->build(From => $MAIL_FROM,
				   To => $MAIL_ANNOUNCE_TO,
				   Cc => $MAIL_ANNOUNCE_CC,
				   common_mail_header(),
				   Subject => "Linux $FULL_VERSION",
				   Type => 'multipart/mixed',
				   Encoding => '8bit');

my $body = $announce->attach(Type => 'text/plain',
			     Charset => 'UTF-8',
			     Encoding => '8bit',
			     Data => '');
my $body_fh = $body->open('w');
print $body_fh
"I'm announcing the release of the $FULL_VERSION kernel.

All users of the $BASE_VERSION kernel series should upgrade.

The updated $BASE_VERSION.y git tree can be found at:
        https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git linux-$BASE_VERSION.y
and can be browsed at the normal kernel.org git web browser:
        https://git.kernel.org/?p=linux/kernel/git/stable/linux-stable.git

The diff from $LAST_VERSION is attached to this message.

Ben.

------------

";
print $body_fh `git diff --stat v$LAST_VERSION..v$FULL_VERSION`;
print $body_fh "\n";
print $body_fh `git shortlog v$LAST_VERSION..v$FULL_VERSION`;
close $body_fh;

my $diff = $announce->attach(Type => 'text/x-diff',
			     Charset => 'UTF-8',
			     Encoding => '8bit',
			     Disposition => 'attachment',
			     Filename => "linux-$FULL_VERSION.patch",
			     Data => '');
my $diff_fh = $diff->open('w');
print $diff_fh `git diff v$LAST_VERSION..v$FULL_VERSION`;
close $diff_fh;

my $mbox_fh = new FileHandle($mbox, 'w');
mime_entity_to_mbox($announce, $mbox_fh);
close $mbox_fh;
