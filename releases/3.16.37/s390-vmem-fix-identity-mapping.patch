From: Heiko Carstens <heiko.carstens@de.ibm.com>
Date: Tue, 10 May 2016 12:10:22 +0200
Subject: s390/vmem: fix identity mapping

commit c34a69059d7876e0793eb410deedfb08ccb22b02 upstream.

The identity mapping is suboptimal for the last 2GB frame. The mapping
will be established with a mix of 4KB and 1MB mappings instead of a
single 2GB mapping.

This happens because of a off-by-one bug introduced with
commit 50be63450728 ("s390/mm: Convert bootmem to memblock").

Currently the identity mapping looks like this:

0x0000000080000000-0x0000000180000000        4G PUD RW
0x0000000180000000-0x00000001fff00000     2047M PMD RW
0x00000001fff00000-0x0000000200000000        1M PTE RW

With the bug fixed it looks like this:

0x0000000080000000-0x0000000200000000        6G PUD RW

Fixes: 50be63450728 ("s390/mm: Convert bootmem to memblock")
Signed-off-by: Heiko Carstens <heiko.carstens@de.ibm.com>
Signed-off-by: Martin Schwidefsky <schwidefsky@de.ibm.com>
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 arch/s390/mm/vmem.c | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

--- a/arch/s390/mm/vmem.c
+++ b/arch/s390/mm/vmem.c
@@ -380,7 +380,7 @@ void __init vmem_map_init(void)
 	ro_end = (unsigned long)&_eshared & PAGE_MASK;
 	for_each_memblock(memory, reg) {
 		start = reg->base;
-		end = reg->base + reg->size - 1;
+		end = reg->base + reg->size;
 		if (start >= ro_end || end <= ro_start)
 			vmem_add_mem(start, end - start, 0);
 		else if (start >= ro_start && end <= ro_end)
