From: Felipe Balbi <felipe.balbi@linux.intel.com>
Date: Tue, 20 Dec 2016 14:14:40 +0200
Subject: usb: dwc3: gadget: always unmap EP0 requests

commit d62145929992f331fdde924d5963ab49588ccc7d upstream.

commit 0416e494ce7d ("usb: dwc3: ep0: correct cache
sync issue in case of ep0_bounced") introduced a bug
where we would leak DMA resources which would cause
us to starve the system of them resulting in failing
DMA transfers.

Fix the bug by making sure that we always unmap EP0
requests since those are *always* mapped.

Fixes: 0416e494ce7d ("usb: dwc3: ep0: correct cache
	sync issue in case of ep0_bounced")
Tested-by: Tomasz Medrek <tomaszx.medrek@intel.com>
Reported-by: Janusz Dziedzic <januszx.dziedzic@linux.intel.com>
Signed-off-by: Felipe Balbi <felipe.balbi@linux.intel.com>
[bwh: Backported to 3.16: adjust context]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 drivers/usb/dwc3/gadget.c | 8 ++++----
 1 file changed, 4 insertions(+), 4 deletions(-)

--- a/drivers/usb/dwc3/gadget.c
+++ b/drivers/usb/dwc3/gadget.c
@@ -257,11 +257,11 @@ void dwc3_gadget_giveback(struct dwc3_ep
 	if (req->request.status == -EINPROGRESS)
 		req->request.status = status;
 
-	if (dwc->ep0_bounced && dep->number == 0)
+	if (dwc->ep0_bounced && dep->number <= 1)
 		dwc->ep0_bounced = false;
-	else
-		usb_gadget_unmap_request(&dwc->gadget, &req->request,
-				req->direction);
+
+	usb_gadget_unmap_request(&dwc->gadget,
+			&req->request, req->direction);
 
 	dev_dbg(dwc->dev, "request %p from %s completed %d/%d ===> %d\n",
 			req, dep->name, req->request.actual,
