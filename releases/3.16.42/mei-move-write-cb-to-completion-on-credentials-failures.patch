From: Alexander Usyskin <alexander.usyskin@intel.com>
Date: Wed, 14 Dec 2016 17:56:52 +0200
Subject: mei: move write cb to completion on credentials failures

commit e09ee853c92011860a4bd2fbdf6126f60fc16bd3 upstream.

The credentials handling was pushed to the write handlers
but error handling wasn't done properly.
Move write callbacks to completion queue to destroy them
and to notify a blocked writer about the failure

Fixes: 136698e535cd1 (mei: push credentials inside the irq write handler)
Signed-off-by: Alexander Usyskin <alexander.usyskin@intel.com>
Signed-off-by: Tomas Winkler <tomas.winkler@intel.com>
Signed-off-by: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
[bwh: Backported to 3.16: adjust context]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 drivers/misc/mei/client.c | 20 ++++++++++++--------
 1 file changed, 12 insertions(+), 8 deletions(-)

--- a/drivers/misc/mei/client.c
+++ b/drivers/misc/mei/client.c
@@ -819,7 +819,7 @@ int mei_cl_irq_write(struct mei_cl *cl,
 
 	rets = mei_cl_flow_ctrl_creds(cl);
 	if (rets < 0)
-		return rets;
+		goto err;
 
 	if (rets == 0) {
 		cl_dbg(dev, cl, "No flow control credentials: not sending.\n");
@@ -853,23 +853,27 @@ int mei_cl_irq_write(struct mei_cl *cl,
 			cb->request_buffer.size, cb->buf_idx);
 
 	rets = mei_write_message(dev, &mei_hdr, buf->data + cb->buf_idx);
-	if (rets) {
-		cl->status = rets;
-		list_move_tail(&cb->list, &cmpl_list->list);
-		return rets;
-	}
+	if (rets)
+		goto err;
 
 	cl->status = 0;
 	cl->writing_state = MEI_WRITING;
 	cb->buf_idx += mei_hdr.length;
 
 	if (mei_hdr.msg_complete) {
-		if (mei_cl_flow_ctrl_reduce(cl))
-			return -EIO;
+		if (mei_cl_flow_ctrl_reduce(cl)) {
+			rets = -EIO;
+			goto err;
+		}
 		list_move_tail(&cb->list, &dev->write_waiting_list.list);
 	}
 
 	return 0;
+
+err:
+	cl->status = rets;
+	list_move_tail(&cb->list, &cmpl_list->list);
+	return rets;
 }
 
 /**
