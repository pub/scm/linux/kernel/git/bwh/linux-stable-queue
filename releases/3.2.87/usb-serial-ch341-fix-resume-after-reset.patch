From: Johan Hovold <johan@kernel.org>
Date: Fri, 6 Jan 2017 19:15:14 +0100
Subject: USB: serial: ch341: fix resume after reset

commit ce5e292828117d1b71cbd3edf9e9137cf31acd30 upstream.

Fix reset-resume handling which failed to resubmit the read and
interrupt URBs, thereby leaving a port that was open before suspend in a
broken state until closed and reopened.

Fixes: 1ded7ea47b88 ("USB: ch341 serial: fix port number changed after
resume")
Fixes: 2bfd1c96a9fb ("USB: serial: ch341: remove reset_resume callback")
Signed-off-by: Johan Hovold <johan@kernel.org>
[bwh: Backported to 3.2:
 - Move initialisation of 'serial' up to make this work
 - Delete the call to usb_serial_resume() that was still present and
   would be redundant with usb_serial_generic_resume()
 - Open-code tty_port_initialized()]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
--- a/drivers/usb/serial/ch341.c
+++ b/drivers/usb/serial/ch341.c
@@ -621,18 +621,24 @@ static int ch341_tiocmget(struct tty_str
 static int ch341_reset_resume(struct usb_interface *intf)
 {
 	struct usb_device *dev = interface_to_usbdev(intf);
-	struct usb_serial *serial = NULL;
-	struct ch341_private *priv;
-
-	serial = usb_get_intfdata(intf);
-	priv = usb_get_serial_port_data(serial->port[0]);
+	struct usb_serial *serial = usb_get_intfdata(intf);
+	struct usb_serial_port *port = serial->port[0];
+	struct ch341_private *priv = usb_get_serial_port_data(port);
+	int ret;
 
 	/*reconfigure ch341 serial port after bus-reset*/
 	ch341_configure(dev, priv);
 
-	usb_serial_resume(intf);
+	if (port->port.flags & ASYNC_INITIALIZED) {
+		ret = usb_submit_urb(port->interrupt_in_urb, GFP_NOIO);
+		if (ret) {
+			dev_err(&port->dev, "failed to submit interrupt urb: %d\n",
+				ret);
+			return ret;
+		}
+	}
 
-	return 0;
+	return usb_serial_generic_resume(serial);
 }
 
 static struct usb_driver ch341_driver = {
