From: Dan Carpenter <dan.carpenter@oracle.com>
Date: Fri, 9 Jan 2015 15:32:31 +0300
Subject: HID: roccat: potential out of bounds in pyra_sysfs_write_settings()

commit 606185b20caf4c57d7e41e5a5ea4aff460aef2ab upstream.

This is a static checker fix.  We write some binary settings to the
sysfs file.  One of the settings is the "->startup_profile".  There
isn't any checking to make sure it fits into the
pyra->profile_settings[] array in the profile_activated() function.

I added a check to pyra_sysfs_write_settings() in both places because
I wasn't positive that the other callers were correct.

Signed-off-by: Dan Carpenter <dan.carpenter@oracle.com>
Signed-off-by: Jiri Kosina <jkosina@suse.cz>
[bwh: Backported to 3.2: pyra_sysfs_write_settings() doesn't define a
 settings variable, so write the cast-expression inline]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 drivers/hid/hid-roccat-pyra.c | 8 ++++++--
 1 file changed, 6 insertions(+), 2 deletions(-)

--- a/drivers/hid/hid-roccat-pyra.c
+++ b/drivers/hid/hid-roccat-pyra.c
@@ -35,6 +35,8 @@ static struct class *pyra_class;
 static void profile_activated(struct pyra_device *pyra,
 		unsigned int new_profile)
 {
+	if (new_profile >= ARRAY_SIZE(pyra->profile_settings))
+		return;
 	pyra->actual_profile = new_profile;
 	pyra->actual_cpi = pyra->profile_settings[pyra->actual_profile].y_cpi;
 }
@@ -303,6 +305,10 @@ static ssize_t pyra_sysfs_write_settings
 	if (off != 0 || count != sizeof(struct pyra_settings))
 		return -EINVAL;
 
+	if (((struct pyra_settings const *)buf)->startup_profile >=
+	    ARRAY_SIZE(pyra->profile_settings))
+		return -EINVAL;
+
 	mutex_lock(&pyra->pyra_lock);
 	difference = memcmp(buf, &pyra->settings, sizeof(struct pyra_settings));
 	if (difference) {
