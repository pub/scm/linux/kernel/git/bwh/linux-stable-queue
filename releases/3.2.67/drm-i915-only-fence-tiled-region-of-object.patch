From: Bob Paauwe <bob.j.paauwe@intel.com>
Date: Thu, 18 Dec 2014 09:51:26 -0800
Subject: drm/i915: Only fence tiled region of object.

commit af1a7301c7cf8912dca03065d448c4437c5c239f upstream.

When creating a fence for a tiled object, only fence the area that
makes up the actual tiles.  The object may be larger than the tiled
area and if we allow those extra addresses to be fenced, they'll
get converted to addresses beyond where the object is mapped. This
opens up the possiblity of writes beyond the end of object.

To prevent this, we adjust the size of the fence to only encompass
the area that makes up the actual tiles.  The extra space is considered
un-tiled and now behaves as if it was a linear object.

Testcase: igt/gem_tiled_fence_overflow
Reported-by: Dan Hettena <danh@ghs.com>
Signed-off-by: Bob Paauwe <bob.j.paauwe@intel.com>
Reviewed-by: Daniel Vetter <daniel.vetter@ffwll.ch>
Signed-off-by: Jani Nikula <jani.nikula@intel.com>
[bwh: Backported to 3.2:
 - Adjust context, indentation
 - Apply to both i965_write_fence_reg() and sandybridge_write_fence_reg(),
   which have been combined into one function upstream]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 drivers/gpu/drm/i915/i915_gem.c | 7 +++++++
 1 file changed, 7 insertions(+)

--- a/drivers/gpu/drm/i915/i915_gem.c
+++ b/drivers/gpu/drm/i915/i915_gem.c
@@ -2248,6 +2248,13 @@ static int sandybridge_write_fence_reg(s
 	int regnum = obj->fence_reg;
 	uint64_t val;
 
+	/* Adjust fence size to match tiled area */
+	if (obj->tiling_mode != I915_TILING_NONE) {
+		uint32_t row_size = obj->stride *
+			(obj->tiling_mode == I915_TILING_Y ? 32 : 8);
+		size = (size / row_size) * row_size;
+	}
+
 	val = (uint64_t)((obj->gtt_offset + size - 4096) &
 			 0xfffff000) << 32;
 	val |= obj->gtt_offset & 0xfffff000;
@@ -2285,6 +2292,13 @@ static int i965_write_fence_reg(struct d
 	int regnum = obj->fence_reg;
 	uint64_t val;
 
+	/* Adjust fence size to match tiled area */
+	if (obj->tiling_mode != I915_TILING_NONE) {
+		uint32_t row_size = obj->stride *
+			(obj->tiling_mode == I915_TILING_Y ? 32 : 8);
+		size = (size / row_size) * row_size;
+	}
+
 	val = (uint64_t)((obj->gtt_offset + size - 4096) &
 		    0xfffff000) << 32;
 	val |= obj->gtt_offset & 0xfffff000;
