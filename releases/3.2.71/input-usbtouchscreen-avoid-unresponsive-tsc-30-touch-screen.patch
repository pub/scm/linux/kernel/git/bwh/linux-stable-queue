From: Bernhard Bender <bernhard.bender@bytecmed.com>
Date: Thu, 23 Jul 2015 13:58:08 -0700
Subject: Input: usbtouchscreen - avoid unresponsive TSC-30 touch screen

commit 968491709e5b1aaf429428814fff3d932fa90b60 upstream.

This patch fixes a problem in the usbtouchscreen driver for DMC TSC-30
touch screen.  Due to a missing delay between the RESET and SET_RATE
commands, the touch screen may become unresponsive during system startup or
driver loading.

According to the DMC documentation, a delay is needed after the RESET
command to allow the chip to complete its internal initialization. As this
delay is not guaranteed, we had a system where the touch screen
occasionally did not send any touch data. There was no other indication of
the problem.

The patch fixes the problem by adding a 150ms delay between the RESET and
SET_RATE commands.

Suggested-by: Jakob Mustafa <jakob.mustafa@bytecmed.com>
Signed-off-by: Bernhard Bender <bernhard.bender@bytecmed.com>
Signed-off-by: Dmitry Torokhov <dmitry.torokhov@gmail.com>
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 drivers/input/touchscreen/usbtouchscreen.c | 3 +++
 1 file changed, 3 insertions(+)

--- a/drivers/input/touchscreen/usbtouchscreen.c
+++ b/drivers/input/touchscreen/usbtouchscreen.c
@@ -528,6 +528,9 @@ static int dmc_tsc10_init(struct usbtouc
 		goto err_out;
 	}
 
+	/* TSC-25 data sheet specifies a delay after the RESET command */
+	msleep(150);
+
 	/* set coordinate output rate */
 	buf[0] = buf[1] = 0xFF;
 	ret = usb_control_msg(dev, usb_rcvctrlpipe (dev, 0),
