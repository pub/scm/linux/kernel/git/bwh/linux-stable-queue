From: Eric Biggers <ebiggers@google.com>
Date: Wed, 21 Aug 2019 22:54:41 -0700
Subject: smack: use GFP_NOFS while holding inode_smack::smk_lock

commit e5bfad3d7acc5702f32aafeb388362994f4d7bd0 upstream.

inode_smack::smk_lock is taken during smack_d_instantiate(), which is
called during a filesystem transaction when creating a file on ext4.
Therefore to avoid a deadlock, all code that takes this lock must use
GFP_NOFS, to prevent memory reclaim from waiting for the filesystem
transaction to complete.

Reported-by: syzbot+0eefc1e06a77d327a056@syzkaller.appspotmail.com
Signed-off-by: Eric Biggers <ebiggers@google.com>
Signed-off-by: Casey Schaufler <casey@schaufler-ca.com>
[bwh: Backported to 3.16:
 - Drop change to smk_netlbl_mls(), where GFP_ATOMIC is used
 - Adjust context]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
--- a/security/smack/smack_access.c
+++ b/security/smack/smack_access.c
@@ -430,7 +430,7 @@ char *smk_parse_smack(const char *string
 	if (i == 0 || i >= SMK_LONGLABEL)
 		return NULL;
 
-	smack = kzalloc(i + 1, GFP_KERNEL);
+	smack = kzalloc(i + 1, GFP_NOFS);
 	if (smack != NULL) {
 		strncpy(smack, string, i + 1);
 		smack[i] = '\0';
@@ -502,7 +502,7 @@ struct smack_known *smk_import_entry(con
 	if (skp != NULL)
 		goto freeout;
 
-	skp = kzalloc(sizeof(*skp), GFP_KERNEL);
+	skp = kzalloc(sizeof(*skp), GFP_NOFS);
 	if (skp == NULL)
 		goto freeout;
 
--- a/security/smack/smack_lsm.c
+++ b/security/smack/smack_lsm.c
@@ -70,7 +70,7 @@ static struct smack_known *smk_fetch(con
 	if (ip->i_op->getxattr == NULL)
 		return NULL;
 
-	buffer = kzalloc(SMK_LONGLABEL, GFP_KERNEL);
+	buffer = kzalloc(SMK_LONGLABEL, GFP_NOFS);
 	if (buffer == NULL)
 		return NULL;
 
