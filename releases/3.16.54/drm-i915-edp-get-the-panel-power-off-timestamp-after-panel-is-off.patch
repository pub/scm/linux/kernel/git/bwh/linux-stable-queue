From: Manasi Navare <manasi.d.navare@intel.com>
Date: Wed, 4 Oct 2017 09:48:26 -0700
Subject: drm/i915/edp: Get the Panel Power Off timestamp after panel is off

commit cbacf02e7796fea02e5c6e46c90ed7cbe9e6f2c0 upstream.

Kernel stores the time in jiffies at which the eDP panel is turned
off. This should be obtained after the panel is off (after the
wait_panel_off). When we next attempt to turn the panel on, we use the
difference between the timestamp at which we want to turn the panel on
and timestamp at which panel was turned off to ensure that this is equal
to panel power cycle delay and if not we wait for the remaining
time. Not waiting for the panel power cycle delay can cause the panel to
not turn on giving rise to AUX timeouts for the attempted AUX
transactions.

v2:
* Separate lines for bugzilla (Jani Nikula)
* Suggested by tag (Daniel Vetter)

Cc: Daniel Vetter <daniel.vetter@ffwll.ch>
Cc: Jani Nikula <jani.nikula@linux.intel.com>
Bugzilla: https://bugs.freedesktop.org/show_bug.cgi?id=101518
Bugzilla: https://bugs.freedesktop.org/show_bug.cgi?id=101144
Suggested-by: Daniel Vetter <daniel.vetter@ffwll.ch>
Signed-off-by: Manasi Navare <manasi.d.navare@intel.com>
Reviewed-by: Daniel Vetter <daniel.vetter@ffwll.ch>
Reviewed-by: Jani Nikula <jani.nikula@linux.intel.com>
Signed-off-by: Jani Nikula <jani.nikula@intel.com>
Link: https://patchwork.freedesktop.org/patch/msgid/1507135706-17147-1-git-send-email-manasi.d.navare@intel.com
[bwh: Backported to 3.16: We really do use jiffies here, and the field in
 struct intel_dp has a different name]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 drivers/gpu/drm/i915/intel_dp.c | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

--- a/drivers/gpu/drm/i915/intel_dp.c
+++ b/drivers/gpu/drm/i915/intel_dp.c
@@ -1365,8 +1365,8 @@ void intel_edp_panel_off(struct intel_dp
 	I915_WRITE(pp_ctrl_reg, pp);
 	POSTING_READ(pp_ctrl_reg);
 
-	intel_dp->last_power_cycle = jiffies;
 	wait_panel_off(intel_dp);
+	intel_dp->last_power_cycle = jiffies;
 
 	/* We got a reference when we enabled the VDD. */
 	power_domain = intel_display_port_power_domain(intel_encoder);
