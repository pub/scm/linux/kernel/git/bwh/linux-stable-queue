From: Jan Kara <jack@suse.cz>
Date: Wed, 2 Aug 2017 13:32:30 -0700
Subject: ocfs2: don't clear SGID when inheriting ACLs

commit 19ec8e48582670c021e998b9deb88e39a842ff45 upstream.

When new directory 'DIR1' is created in a directory 'DIR0' with SGID bit
set, DIR1 is expected to have SGID bit set (and owning group equal to
the owning group of 'DIR0').  However when 'DIR0' also has some default
ACLs that 'DIR1' inherits, setting these ACLs will result in SGID bit on
'DIR1' to get cleared if user is not member of the owning group.

Fix the problem by moving posix_acl_update_mode() out of ocfs2_set_acl()
into ocfs2_iop_set_acl().  That way the function will not be called when
inheriting ACLs which is what we want as it prevents SGID bit clearing
and the mode has been properly set by posix_acl_create() anyway.  Also
posix_acl_chmod() that is calling ocfs2_set_acl() takes care of updating
mode itself.

Fixes: 073931017b4 ("posix_acl: Clear SGID bit when setting file permissions")
Link: http://lkml.kernel.org/r/20170801141252.19675-3-jack@suse.cz
Signed-off-by: Jan Kara <jack@suse.cz>
Cc: Mark Fasheh <mfasheh@versity.com>
Cc: Joel Becker <jlbec@evilplan.org>
Cc: Junxiao Bi <junxiao.bi@oracle.com>
Cc: Joseph Qi <jiangqi903@gmail.com>
Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
Signed-off-by: Linus Torvalds <torvalds@linux-foundation.org>
[bwh: Backported to 3.2: Move the call to posix_acl_update_mode() into
 ocfs2_xattr_set_acl(). Pass NULL as the bh argument to
 ocfs2_acl_set_mode(). Reuse the existing cleanup label.]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
--- a/fs/ocfs2/acl.c
+++ b/fs/ocfs2/acl.c
@@ -246,19 +246,6 @@ static int ocfs2_set_acl(handle_t *handl
 	switch (type) {
 	case ACL_TYPE_ACCESS:
 		name_index = OCFS2_XATTR_INDEX_POSIX_ACL_ACCESS;
-		if (acl) {
-			umode_t mode;
-			ret = posix_acl_update_mode(inode, &mode, &acl);
-			if (ret)
-				return ret;
-			else {
-				ret = ocfs2_acl_set_mode(inode, di_bh,
-							 handle, mode);
-				if (ret)
-					return ret;
-
-			}
-		}
 		break;
 	case ACL_TYPE_DEFAULT:
 		name_index = OCFS2_XATTR_INDEX_POSIX_ACL_DEFAULT;
@@ -483,6 +470,17 @@ static int ocfs2_xattr_set_acl(struct de
 	} else
 		acl = NULL;
 
+	if (type == ACL_TYPE_ACCESS && acl) {
+		umode_t mode;
+
+		ret = posix_acl_update_mode(inode, &mode, &acl);
+		if (ret)
+			goto cleanup;
+
+		ret = ocfs2_acl_set_mode(inode, NULL, NULL, mode);
+		if (ret)
+			goto cleanup;
+	}
 	ret = ocfs2_set_acl(NULL, inode, NULL, type, acl, NULL, NULL);
 
 cleanup:
