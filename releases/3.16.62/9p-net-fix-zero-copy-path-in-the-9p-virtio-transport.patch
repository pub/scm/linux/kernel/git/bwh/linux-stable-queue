From: Chirantan Ekbote <chirantan@chromium.org>
Date: Mon, 16 Jul 2018 17:35:29 -0700
Subject: 9p/net: Fix zero-copy path in the 9p virtio transport

commit d28c756caee6e414d9ba367d0b92da24145af2a8 upstream.

The zero-copy optimization when reading or writing large chunks of data
is quite useful.  However, the 9p messages created through the zero-copy
write path have an incorrect message size: it should be the size of the
header + size of the data being written but instead it's just the size
of the header.

This only works if the server ignores the size field of the message and
otherwise breaks the framing of the protocol. Fix this by re-writing the
message size field with the correct value.

Tested by running `dd if=/dev/zero of=out bs=4k count=1` inside a
virtio-9p mount.

Link: http://lkml.kernel.org/r/20180717003529.114368-1-chirantan@chromium.org
Signed-off-by: Chirantan Ekbote <chirantan@chromium.org>
Reviewed-by: Greg Kurz <groug@kaod.org>
Tested-by: Greg Kurz <groug@kaod.org>
Cc: Dylan Reid <dgreid@chromium.org>
Cc: Guenter Roeck <groeck@chromium.org>
Signed-off-by: Dominique Martinet <dominique.martinet@cea.fr>
[bwh: Backported to 3.16: adjust context]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 net/9p/trans_virtio.c | 7 +++++++
 1 file changed, 7 insertions(+)

--- a/net/9p/trans_virtio.c
+++ b/net/9p/trans_virtio.c
@@ -378,6 +378,7 @@ p9_virtio_zc_request(struct p9_client *c
 	p9_debug(P9_DEBUG_TRANS, "virtio request\n");
 
 	if (uodata) {
+		__le32 sz;
 		out_nr_pages = p9_nr_pages(uodata, outlen);
 		out_pages = kmalloc(sizeof(struct page *) * out_nr_pages,
 				    GFP_NOFS);
@@ -393,6 +394,12 @@ p9_virtio_zc_request(struct p9_client *c
 			out_pages = NULL;
 			goto err_out;
 		}
+		/* The size field of the message must include the length of the
+		 * header and the length of the data.  We didn't actually know
+		 * the length of the data until this point so add it in now.
+		 */
+		sz = cpu_to_le32(req->tc->size + outlen);
+		memcpy(&req->tc->sdata[0], &sz, sizeof(sz));
 	}
 	if (uidata) {
 		in_nr_pages = p9_nr_pages(uidata, inlen);
