From: Theodore Ts'o <tytso@mit.edu>
Date: Mon, 27 Aug 2018 01:47:09 -0400
Subject: ext4: check to make sure the rename(2)'s destination is not freed

commit b50282f3241acee880514212d88b6049fb5039c8 upstream.

If the destination of the rename(2) system call exists, the inode's
link count (i_nlinks) must be non-zero.  If it is, the inode can end
up on the orphan list prematurely, leading to all sorts of hilarity,
including a use-after-free.

https://bugzilla.kernel.org/show_bug.cgi?id=200931

Signed-off-by: Theodore Ts'o <tytso@mit.edu>
Reported-by: Wen Xu <wen.xu@gatech.edu>
[bwh: Backported to 3.16:
 - Return -EIO on error
 - Adjust context]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 fs/ext4/namei.c | 6 ++++++
 1 file changed, 6 insertions(+)

--- a/fs/ext4/namei.c
+++ b/fs/ext4/namei.c
@@ -3211,6 +3211,12 @@ static int ext4_rename(struct inode *old
 	int force_reread;
 	int retval;
 
+	if (new.inode && new.inode->i_nlink == 0) {
+		EXT4_ERROR_INODE(new.inode,
+				 "target of rename is already freed");
+		return -EIO;
+	}
+
 	dquot_initialize(old.dir);
 	dquot_initialize(new.dir);
 
