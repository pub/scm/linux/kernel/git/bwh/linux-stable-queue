Date: Tue, 22 Jan 2013 17:43:40 +0100
From: Takashi Iwai <tiwai@suse.de>
Subject: ALSA: usb-audio: Fix regression by disconnection-race-fix  patch

[NOTE: the regression below is found only in 3.2-3.4 stable trees, so
       there is no upstream commit corresponding to this patch]

The recent fix for the race at disconnection of usb-audio devices
(upstream commit 978520b7) triggers Oops when a device is unplugged
while playing on 3.2 and 3.4 kernels.  The culprit is that the
shutdown flag check was wrongly added around the urb deactivation code
snippet.  The urb deactivation code has to be performed even after the
device disconnected.  Otherwise it remains undead and pokes the wild
access in the end.

The regression fix is simply reverting the shutdown flag check in that
code.

Reported-and-tested-by: Chris J Arges <christopherarges@gmail.com>
Signed-off-by: Takashi Iwai <tiwai@suse.de>
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 sound/usb/endpoint.c | 6 ++----
 1 file changed, 2 insertions(+), 4 deletions(-)

diff --git a/sound/usb/endpoint.c b/sound/usb/endpoint.c
index 24c5114..9ab2b3e 100644
--- a/sound/usb/endpoint.c
+++ b/sound/usb/endpoint.c
@@ -148,10 +148,8 @@ void snd_usb_release_substream_urbs(struct snd_usb_substream *subs, int force)
 	int i;
 
 	/* stop urbs (to be sure) */
-	if (!subs->stream->chip->shutdown) {
-		deactivate_urbs(subs, force, 1);
-		wait_clear_urbs(subs);
-	}
+	deactivate_urbs(subs, force, 1);
+	wait_clear_urbs(subs);
 
 	for (i = 0; i < MAX_URBS; i++)
 		release_urb_ctx(&subs->dataurb[i]);
