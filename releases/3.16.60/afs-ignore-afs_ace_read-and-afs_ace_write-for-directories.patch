From: Marc Dionne <marc.dionne@auristor.com>
Date: Thu, 6 Jul 2017 15:50:18 +0100
Subject: afs: Ignore AFS_ACE_READ and AFS_ACE_WRITE for directories

commit fd2498211a551fd42b2d6b9050d649d43536e75c upstream.

The AFS_ACE_READ and AFS_ACE_WRITE permission bits should not
be used to make access decisions for the directory itself.  They
are meant to control access for the objects contained in that
directory.

Reading a directory is allowed if the AFS_ACE_LOOKUP bit is set.
This would cause an incorrect access denied error for a directory
with AFS_ACE_LOOKUP but not AFS_ACE_READ.

The AFS_ACE_WRITE bit does not allow operations that modify the
directory.  For a directory with AFS_ACE_WRITE but neither
AFS_ACE_INSERT nor AFS_ACE_DELETE, this would result in trying
operations that would ultimately be denied by the server.

Signed-off-by: Marc Dionne <marc.dionne@auristor.com>
Signed-off-by: David Howells <dhowells@redhat.com>
Signed-off-by: Linus Torvalds <torvalds@linux-foundation.org>
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 fs/afs/security.c | 5 ++---
 1 file changed, 2 insertions(+), 3 deletions(-)

--- a/fs/afs/security.c
+++ b/fs/afs/security.c
@@ -327,12 +327,11 @@ int afs_permission(struct inode *inode,
 			if (!(access & AFS_ACE_LOOKUP))
 				goto permission_denied;
 		} else if (mask & MAY_READ) {
-			if (!(access & AFS_ACE_READ))
+			if (!(access & AFS_ACE_LOOKUP))
 				goto permission_denied;
 		} else if (mask & MAY_WRITE) {
 			if (!(access & (AFS_ACE_DELETE | /* rmdir, unlink, rename from */
-					AFS_ACE_INSERT | /* create, mkdir, symlink, rename to */
-					AFS_ACE_WRITE))) /* chmod */
+					AFS_ACE_INSERT))) /* create, mkdir, symlink, rename to */
 				goto permission_denied;
 		} else {
 			BUG();
