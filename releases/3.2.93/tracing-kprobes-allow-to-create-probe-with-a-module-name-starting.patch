From: Sabrina Dubroca <sd@queasysnail.net>
Date: Thu, 22 Jun 2017 11:24:42 +0200
Subject: tracing/kprobes: Allow to create probe with a module name starting
 with a digit

commit 9e52b32567126fe146f198971364f68d3bc5233f upstream.

Always try to parse an address, since kstrtoul() will safely fail when
given a symbol as input. If that fails (which will be the case for a
symbol), try to parse a symbol instead.

This allows creating a probe such as:

    p:probe/vlan_gro_receive 8021q:vlan_gro_receive+0

Which is necessary for this command to work:

    perf probe -m 8021q -a vlan_gro_receive

Link: http://lkml.kernel.org/r/fd72d666f45b114e2c5b9cf7e27b91de1ec966f1.1498122881.git.sd@queasysnail.net

Fixes: 413d37d1e ("tracing: Add kprobe-based event tracer")
Acked-by: Masami Hiramatsu <mhiramat@kernel.org>
Signed-off-by: Sabrina Dubroca <sd@queasysnail.net>
Signed-off-by: Steven Rostedt (VMware) <rostedt@goodmis.org>
[bwh: Backported to 3.2: preserve the check that an addresses isn't used for
 a kretprobe]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 kernel/trace/trace_kprobe.c | 14 +++++---------
 1 file changed, 5 insertions(+), 9 deletions(-)

--- a/kernel/trace/trace_kprobe.c
+++ b/kernel/trace/trace_kprobe.c
@@ -1225,24 +1225,21 @@ static int create_trace_probe(int argc,
 		pr_info("Probe point is not specified.\n");
 		return -EINVAL;
 	}
-	if (isdigit(argv[1][0])) {
+
+	/* try to parse an address. if that fails, try to read the
+	 * input as a symbol. */
+	if (!strict_strtoul(argv[1], 0, (unsigned long *)&addr)) {
 		if (is_return) {
 			pr_info("Return probe point must be a symbol.\n");
 			return -EINVAL;
 		}
-		/* an address specified */
-		ret = strict_strtoul(&argv[1][0], 0, (unsigned long *)&addr);
-		if (ret) {
-			pr_info("Failed to parse address.\n");
-			return ret;
-		}
 	} else {
 		/* a symbol specified */
 		symbol = argv[1];
 		/* TODO: support .init module functions */
 		ret = split_symbol_offset(symbol, &offset);
 		if (ret) {
-			pr_info("Failed to parse symbol.\n");
+			pr_info("Failed to parse either an address or a symbol.\n");
 			return ret;
 		}
 		if (offset && is_return) {
