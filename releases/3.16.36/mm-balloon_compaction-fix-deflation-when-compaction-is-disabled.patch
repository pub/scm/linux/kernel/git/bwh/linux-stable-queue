From: Konstantin Khlebnikov <k.khlebnikov@samsung.com>
Date: Wed, 29 Oct 2014 14:51:02 -0700
Subject: mm/balloon_compaction: fix deflation when compaction is disabled

commit 4d88e6f7d5ffc84e6094a47925870f4a130555c2 upstream.

If CONFIG_BALLOON_COMPACTION=n balloon_page_insert() does not link pages
with balloon and doesn't set PagePrivate flag, as a result
balloon_page_dequeue() cannot get any pages because it thinks that all
of them are isolated.  Without balloon compaction nobody can isolate
ballooned pages.  It's safe to remove this check.

Fixes: d6d86c0a7f8d ("mm/balloon_compaction: redesign ballooned pages management").
Signed-off-by: Konstantin Khlebnikov <k.khlebnikov@samsung.com>
Reported-by: Matt Mullins <mmullins@mmlx.us>
Signed-off-by: Andrew Morton <akpm@linux-foundation.org>
Signed-off-by: Linus Torvalds <torvalds@linux-foundation.org>
Signed-off-by: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
[bwh: Backported to 3.16: adjust context]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
Cc: Jiri Kosina <jkosina@suse.cz>
Cc: jian wang <wangjian@bytedance.com>
---
 mm/balloon_compaction.c | 2 ++
 1 file changed, 2 insertions(+)

--- a/mm/balloon_compaction.c
+++ b/mm/balloon_compaction.c
@@ -94,11 +94,13 @@ struct page *balloon_page_dequeue(struct
 		 * to be released by the balloon driver.
 		 */
 		if (trylock_page(page)) {
+#ifdef CONFIG_BALLOON_COMPACTION
 			if (!PagePrivate(page)) {
 				/* raced with isolation */
 				unlock_page(page);
 				continue;
 			}
+#endif
 			balloon_page_delete(page);
 			unlock_page(page);
 			dequeued_page = true;
