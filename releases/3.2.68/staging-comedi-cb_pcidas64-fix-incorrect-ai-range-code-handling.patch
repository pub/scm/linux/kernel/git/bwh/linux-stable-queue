From: Ian Abbott <abbotti@mev.co.uk>
Date: Fri, 27 Feb 2015 18:52:08 +0000
Subject: staging: comedi: cb_pcidas64: fix incorrect  AI range code handling

commit be8e89087ec2d2c8a1ad1e3db64bf4efdfc3c298 upstream.

The hardware range code values and list of valid ranges for the AI
subdevice is incorrect for several supported boards.  The hardware range
code values for all boards except PCI-DAS4020/12 is determined by
calling `ai_range_bits_6xxx()` based on the maximum voltage of the range
and whether it is bipolar or unipolar, however it only returns the
correct hardware range code for the PCI-DAS60xx boards.  For
PCI-DAS6402/16 (and /12) it returns the wrong code for the unipolar
ranges.  For PCI-DAS64/Mx/16 it returns the wrong code for all the
ranges and the comedi range table is incorrect.

Change `ai_range_bits_6xxx()` to use a look-up table pointed to by new
member `ai_range_codes` of `struct pcidas64_board` to map the comedi
range table indices to the hardware range codes.  Use a new comedi range
table for the PCI-DAS64/Mx/16 boards (and the commented out variants).

Signed-off-by: Ian Abbott <abbotti@mev.co.uk>
Signed-off-by: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 drivers/staging/comedi/drivers/cb_pcidas64.c | 123 ++++++++++++++++-----------
 1 file changed, 75 insertions(+), 48 deletions(-)

--- a/drivers/staging/comedi/drivers/cb_pcidas64.c
+++ b/drivers/staging/comedi/drivers/cb_pcidas64.c
@@ -424,6 +424,29 @@ static const struct comedi_lrange ai_ran
 	 }
 };
 
+static const uint8_t ai_range_code_64xx[8] = {
+	0x0, 0x1, 0x2, 0x3,	/* bipolar 10, 5, 2,5, 1.25 */
+	0x8, 0x9, 0xa, 0xb	/* unipolar 10, 5, 2.5, 1.25 */
+};
+
+/* analog input ranges for 64-Mx boards */
+static const struct comedi_lrange ai_ranges_64_mx = {
+	7, {
+		BIP_RANGE(5),
+		BIP_RANGE(2.5),
+		BIP_RANGE(1.25),
+		BIP_RANGE(0.625),
+		UNI_RANGE(5),
+		UNI_RANGE(2.5),
+		UNI_RANGE(1.25)
+	}
+};
+
+static const uint8_t ai_range_code_64_mx[7] = {
+	0x0, 0x1, 0x2, 0x3,	/* bipolar 5, 2.5, 1.25, 0.625 */
+	0x9, 0xa, 0xb		/* unipolar 5, 2.5, 1.25 */
+};
+
 /* analog input ranges for 60xx boards */
 static const struct comedi_lrange ai_ranges_60xx = {
 	4,
@@ -435,6 +458,10 @@ static const struct comedi_lrange ai_ran
 	 }
 };
 
+static const uint8_t ai_range_code_60xx[4] = {
+	0x0, 0x1, 0x4, 0x7	/* bipolar 10, 5, 0.5, 0.05 */
+};
+
 /* analog input ranges for 6030, etc boards */
 static const struct comedi_lrange ai_ranges_6030 = {
 	14,
@@ -456,6 +483,11 @@ static const struct comedi_lrange ai_ran
 	 }
 };
 
+static const uint8_t ai_range_code_6030[14] = {
+	0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, /* bip 10, 5, 2, 1, 0.5, 0.2, 0.1 */
+	0x9, 0xa, 0xb, 0xc, 0xd, 0xe, 0xf  /* uni 10, 5, 2, 1, 0.5, 0.2, 0.1 */
+};
+
 /* analog input ranges for 6052, etc boards */
 static const struct comedi_lrange ai_ranges_6052 = {
 	15,
@@ -478,6 +510,11 @@ static const struct comedi_lrange ai_ran
 	 }
 };
 
+static const uint8_t ai_range_code_6052[15] = {
+	0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7,	/* bipolar 10 ... 0.05 */
+	0x9, 0xa, 0xb, 0xc, 0xd, 0xe, 0xf	/* unipolar 10 ... 0.1 */
+};
+
 /* analog input ranges for 4020 board */
 static const struct comedi_lrange ai_ranges_4020 = {
 	2,
@@ -562,6 +599,7 @@ struct pcidas64_board {
 	int ai_bits;		/*  analog input resolution */
 	int ai_speed;		/*  fastest conversion period in ns */
 	const struct comedi_lrange *ai_range_table;
+	const uint8_t *ai_range_code;
 	int ao_nchan;		/*  number of analog out channels */
 	int ao_bits;		/*  analog output resolution */
 	int ao_scan_speed;	/*  analog output speed (for a scan, not conversion) */
@@ -620,6 +658,7 @@ static const struct pcidas64_board pcida
 	 .ao_scan_speed = 10000,
 	 .layout = LAYOUT_64XX,
 	 .ai_range_table = &ai_ranges_64xx,
+	 .ai_range_code = ai_range_code_64xx,
 	 .ao_range_table = &ao_ranges_64xx,
 	 .ao_range_code = ao_range_code_64xx,
 	 .ai_fifo = &ai_fifo_64xx,
@@ -636,6 +675,7 @@ static const struct pcidas64_board pcida
 	 .ao_scan_speed = 10000,
 	 .layout = LAYOUT_64XX,
 	 .ai_range_table = &ai_ranges_64xx,
+	 .ai_range_code = ai_range_code_64xx,
 	 .ao_range_table = &ao_ranges_64xx,
 	 .ao_range_code = ao_range_code_64xx,
 	 .ai_fifo = &ai_fifo_64xx,
@@ -651,7 +691,8 @@ static const struct pcidas64_board pcida
 	 .ao_bits = 16,
 	 .ao_scan_speed = 10000,
 	 .layout = LAYOUT_64XX,
-	 .ai_range_table = &ai_ranges_64xx,
+	 .ai_range_table = &ai_ranges_64_mx,
+	 .ai_range_code = ai_range_code_64_mx,
 	 .ao_range_table = &ao_ranges_64xx,
 	 .ao_range_code = ao_range_code_64xx,
 	 .ai_fifo = &ai_fifo_64xx,
@@ -667,7 +708,8 @@ static const struct pcidas64_board pcida
 	 .ao_bits = 16,
 	 .ao_scan_speed = 10000,
 	 .layout = LAYOUT_64XX,
-	 .ai_range_table = &ai_ranges_64xx,
+	 .ai_range_table = &ai_ranges_64_mx,
+	 .ai_range_code = ai_range_code_64_mx,
 	 .ao_range_table = &ao_ranges_64xx,
 	 .ao_range_code = ao_range_code_64xx,
 	 .ai_fifo = &ai_fifo_64xx,
@@ -683,7 +725,8 @@ static const struct pcidas64_board pcida
 	 .ao_bits = 16,
 	 .ao_scan_speed = 10000,
 	 .layout = LAYOUT_64XX,
-	 .ai_range_table = &ai_ranges_64xx,
+	 .ai_range_table = &ai_ranges_64_mx,
+	 .ai_range_code = ai_range_code_64_mx,
 	 .ao_range_table = &ao_ranges_64xx,
 	 .ao_range_code = ao_range_code_64xx,
 	 .ai_fifo = &ai_fifo_64xx,
@@ -699,6 +742,7 @@ static const struct pcidas64_board pcida
 	 .ao_bits = 16,
 	 .layout = LAYOUT_60XX,
 	 .ai_range_table = &ai_ranges_60xx,
+	 .ai_range_code = ai_range_code_60xx,
 	 .ao_range_table = &ao_ranges_60xx,
 	 .ao_range_code = ao_range_code_60xx,
 	 .ai_fifo = &ai_fifo_60xx,
@@ -715,6 +759,7 @@ static const struct pcidas64_board pcida
 	 .ao_scan_speed = 100000,
 	 .layout = LAYOUT_60XX,
 	 .ai_range_table = &ai_ranges_60xx,
+	 .ai_range_code = ai_range_code_60xx,
 	 .ao_range_table = &ao_ranges_60xx,
 	 .ao_range_code = ao_range_code_60xx,
 	 .ai_fifo = &ai_fifo_60xx,
@@ -730,6 +775,7 @@ static const struct pcidas64_board pcida
 	 .ao_scan_speed = 100000,
 	 .layout = LAYOUT_60XX,
 	 .ai_range_table = &ai_ranges_60xx,
+	 .ai_range_code = ai_range_code_60xx,
 	 .ao_range_table = &ao_ranges_60xx,
 	 .ao_range_code = ao_range_code_60xx,
 	 .ai_fifo = &ai_fifo_60xx,
@@ -746,6 +792,7 @@ static const struct pcidas64_board pcida
 	 .ao_scan_speed = 100000,
 	 .layout = LAYOUT_60XX,
 	 .ai_range_table = &ai_ranges_60xx,
+	 .ai_range_code = ai_range_code_60xx,
 	 .ao_range_table = &ao_ranges_60xx,
 	 .ao_range_code = ao_range_code_60xx,
 	 .ai_fifo = &ai_fifo_60xx,
@@ -762,6 +809,7 @@ static const struct pcidas64_board pcida
 	 .ao_scan_speed = 10000,
 	 .layout = LAYOUT_60XX,
 	 .ai_range_table = &ai_ranges_6030,
+	 .ai_range_code = ai_range_code_6030,
 	 .ao_range_table = &ao_ranges_6030,
 	 .ao_range_code = ao_range_code_6030,
 	 .ai_fifo = &ai_fifo_60xx,
@@ -778,6 +826,7 @@ static const struct pcidas64_board pcida
 	 .ao_scan_speed = 10000,
 	 .layout = LAYOUT_60XX,
 	 .ai_range_table = &ai_ranges_6030,
+	 .ai_range_code = ai_range_code_6030,
 	 .ao_range_table = &ao_ranges_6030,
 	 .ao_range_code = ao_range_code_6030,
 	 .ai_fifo = &ai_fifo_60xx,
@@ -792,6 +841,7 @@ static const struct pcidas64_board pcida
 	 .ao_nchan = 0,
 	 .layout = LAYOUT_60XX,
 	 .ai_range_table = &ai_ranges_6030,
+	 .ai_range_code = ai_range_code_6030,
 	 .ai_fifo = &ai_fifo_60xx,
 	 .has_8255 = 0,
 	 },
@@ -804,6 +854,7 @@ static const struct pcidas64_board pcida
 	 .ao_nchan = 0,
 	 .layout = LAYOUT_60XX,
 	 .ai_range_table = &ai_ranges_6030,
+	 .ai_range_code = ai_range_code_6030,
 	 .ai_fifo = &ai_fifo_60xx,
 	 .has_8255 = 0,
 	 },
@@ -817,6 +868,7 @@ static const struct pcidas64_board pcida
 	 .ao_scan_speed = 0,
 	 .layout = LAYOUT_60XX,
 	 .ai_range_table = &ai_ranges_60xx,
+	 .ai_range_code = ai_range_code_60xx,
 	 .ai_fifo = &ai_fifo_60xx,
 	 .has_8255 = 0,
 	 },
@@ -831,6 +883,7 @@ static const struct pcidas64_board pcida
 	 .ao_scan_speed = 100000,
 	 .layout = LAYOUT_60XX,
 	 .ai_range_table = &ai_ranges_60xx,
+	 .ai_range_code = ai_range_code_60xx,
 	 .ao_range_table = &ao_ranges_60xx,
 	 .ao_range_code = ao_range_code_60xx,
 	 .ai_fifo = &ai_fifo_60xx,
@@ -847,6 +900,7 @@ static const struct pcidas64_board pcida
 	 .ao_scan_speed = 100000,
 	 .layout = LAYOUT_60XX,
 	 .ai_range_table = &ai_ranges_60xx,
+	 .ai_range_code = ai_range_code_60xx,
 	 .ao_range_table = &ao_ranges_60xx,
 	 .ao_range_code = ao_range_code_60xx,
 	 .ai_fifo = &ai_fifo_60xx,
@@ -863,6 +917,7 @@ static const struct pcidas64_board pcida
 	 .ao_scan_speed = 1000,
 	 .layout = LAYOUT_60XX,
 	 .ai_range_table = &ai_ranges_6052,
+	 .ai_range_code = ai_range_code_6052,
 	 .ao_range_table = &ao_ranges_6030,
 	 .ao_range_code = ao_range_code_6030,
 	 .ai_fifo = &ai_fifo_60xx,
@@ -879,6 +934,7 @@ static const struct pcidas64_board pcida
 	 .ao_scan_speed = 3333,
 	 .layout = LAYOUT_60XX,
 	 .ai_range_table = &ai_ranges_6052,
+	 .ai_range_code = ai_range_code_6052,
 	 .ao_range_table = &ao_ranges_6030,
 	 .ao_range_code = ao_range_code_6030,
 	 .ai_fifo = &ai_fifo_60xx,
@@ -895,6 +951,7 @@ static const struct pcidas64_board pcida
 	 .ao_scan_speed = 1000,
 	 .layout = LAYOUT_60XX,
 	 .ai_range_table = &ai_ranges_6052,
+	 .ai_range_code = ai_range_code_6052,
 	 .ao_range_table = &ao_ranges_6030,
 	 .ao_range_code = ao_range_code_6030,
 	 .ai_fifo = &ai_fifo_60xx,
@@ -911,6 +968,7 @@ static const struct pcidas64_board pcida
 	 .ao_scan_speed = 1000,
 	 .layout = LAYOUT_60XX,
 	 .ai_range_table = &ai_ranges_6052,
+	 .ai_range_code = ai_range_code_6052,
 	 .ao_range_table = &ao_ranges_6030,
 	 .ao_range_code = ao_range_code_6030,
 	 .ai_fifo = &ai_fifo_60xx,
@@ -943,6 +1001,7 @@ static const struct pcidas64_board pcida
 	 .ao_scan_speed = 10000,
 	 .layout = LAYOUT_64XX,
 	 .ai_range_table = &ai_ranges_64xx,
+	 .ai_range_code = ai_range_code_64xx,
 	 .ai_fifo = ai_fifo_64xx,
 	 .has_8255 = 1,
 	 },
@@ -955,7 +1014,8 @@ static const struct pcidas64_board pcida
 	 .ao_nchan = 0,
 	 .ao_scan_speed = 10000,
 	 .layout = LAYOUT_64XX,
-	 .ai_range_table = &ai_ranges_64xx,
+	 .ai_range_table = &ai_ranges_64_mx,
+	 .ai_range_code = ai_range_code_64_mx,
 	 .ai_fifo = ai_fifo_64xx,
 	 .has_8255 = 1,
 	 },
@@ -968,7 +1028,8 @@ static const struct pcidas64_board pcida
 	 .ao_nchan = 0,
 	 .ao_scan_speed = 10000,
 	 .layout = LAYOUT_64XX,
-	 .ai_range_table = &ai_ranges_64xx,
+	 .ai_range_table = &ai_ranges_64_mx,
+	 .ai_range_code = ai_range_code_64_mx,
 	 .ai_fifo = ai_fifo_64xx,
 	 .has_8255 = 1,
 	 },
@@ -981,7 +1042,8 @@ static const struct pcidas64_board pcida
 	 .ao_nchan = 0,
 	 .ao_scan_speed = 10000,
 	 .layout = LAYOUT_64XX,
-	 .ai_range_table = &ai_ranges_64xx,
+	 .ai_range_table = &ai_ranges_64_mx,
+	 .ai_range_code = ai_range_code_64_mx,
 	 .ai_fifo = ai_fifo_64xx,
 	 .has_8255 = 1,
 	 },
@@ -994,7 +1056,8 @@ static const struct pcidas64_board pcida
 	 .ao_nchan = 2,
 	 .ao_scan_speed = 10000,
 	 .layout = LAYOUT_64XX,
-	 .ai_range_table = &ai_ranges_64xx,
+	 .ai_range_table = &ai_ranges_64_mx,
+	 .ai_range_code = ai_range_code_64_mx,
 	 .ai_fifo = ai_fifo_64xx,
 	 .has_8255 = 1,
 	 },
@@ -1007,7 +1070,8 @@ static const struct pcidas64_board pcida
 	 .ao_nchan = 2,
 	 .ao_scan_speed = 10000,
 	 .layout = LAYOUT_64XX,
-	 .ai_range_table = &ai_ranges_64xx,
+	 .ai_range_table = &ai_ranges_64_mx,
+	 .ai_range_code = ai_range_code_64_mx,
 	 .ai_fifo = ai_fifo_64xx,
 	 .has_8255 = 1,
 	 },
@@ -1020,7 +1084,8 @@ static const struct pcidas64_board pcida
 	 .ao_nchan = 2,
 	 .ao_scan_speed = 10000,
 	 .layout = LAYOUT_64XX,
-	 .ai_range_table = &ai_ranges_64xx,
+	 .ai_range_table = &ai_ranges_64_mx,
+	 .ai_range_code = ai_range_code_64_mx,
 	 .ai_fifo = ai_fifo_64xx,
 	 .has_8255 = 1,
 	 },
@@ -1258,45 +1323,7 @@ module_exit(driver_cb_pcidas_cleanup_mod
 static unsigned int ai_range_bits_6xxx(const struct comedi_device *dev,
 				       unsigned int range_index)
 {
-	const struct comedi_krange *range =
-	    &board(dev)->ai_range_table->range[range_index];
-	unsigned int bits = 0;
-
-	switch (range->max) {
-	case 10000000:
-		bits = 0x000;
-		break;
-	case 5000000:
-		bits = 0x100;
-		break;
-	case 2000000:
-	case 2500000:
-		bits = 0x200;
-		break;
-	case 1000000:
-	case 1250000:
-		bits = 0x300;
-		break;
-	case 500000:
-		bits = 0x400;
-		break;
-	case 200000:
-	case 250000:
-		bits = 0x500;
-		break;
-	case 100000:
-		bits = 0x600;
-		break;
-	case 50000:
-		bits = 0x700;
-		break;
-	default:
-		comedi_error(dev, "bug! in ai_range_bits_6xxx");
-		break;
-	}
-	if (range->min == 0)
-		bits += 0x900;
-	return bits;
+	return board(dev)->ai_range_code[range_index] << 8;
 }
 
 static unsigned int hw_revision(const struct comedi_device *dev,
