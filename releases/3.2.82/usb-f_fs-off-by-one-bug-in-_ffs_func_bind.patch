From: Dan Carpenter <dan.carpenter@oracle.com>
Date: Sat, 28 May 2016 07:48:10 +0300
Subject: usb: f_fs: off by one bug in _ffs_func_bind()

commit 0015f9156092d07b3ec06d37d014328419d5832e upstream.

This loop is supposed to set all the .num[] values to -1 but it's off by
one so it skips the first element and sets one element past the end of
the array.

I've cleaned up the loop a little as well.

Fixes: ddf8abd25994 ('USB: f_fs: the FunctionFS driver')
Acked-by: Michal Nazarewicz <mina86@mina86.com>
Signed-off-by: Dan Carpenter <dan.carpenter@oracle.com>
Signed-off-by: Felipe Balbi <felipe.balbi@linux.intel.com>
[bwh: Backported to 3.2:
 - Adjust filename, context
 - Add 'i' for iteration but don't bother with 'eps_ptr' as the calculation is
   simpler here]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
--- a/drivers/usb/gadget/f_fs.c
+++ b/drivers/usb/gadget/f_fs.c
@@ -2165,7 +2165,7 @@ static int ffs_func_bind(struct usb_conf
 	const int high = gadget_is_dualspeed(func->gadget) &&
 		func->ffs->hs_descs_count;
 
-	int ret;
+	int ret, i;
 
 	/* Make it a single chunk, less management later on */
 	struct {
@@ -2194,8 +2194,8 @@ static int ffs_func_bind(struct usb_conf
 	memset(data->eps, 0, sizeof data->eps);
 	memcpy(data->raw_descs, ffs->raw_descs + 16, sizeof data->raw_descs);
 	memset(data->inums, 0xff, sizeof data->inums);
-	for (ret = ffs->eps_count; ret; --ret)
-		data->eps[ret].num = -1;
+	for (i = 0; i < ffs->eps_count; i++)
+		data->eps[i].num = -1;
 
 	/* Save pointers */
 	func->eps             = data->eps;
