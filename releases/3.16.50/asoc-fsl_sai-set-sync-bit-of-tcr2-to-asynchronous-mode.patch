From: Nicolin Chen <Guangyu.Chen@freescale.com>
Date: Mon, 4 Aug 2014 15:07:25 +0800
Subject: ASoC: fsl_sai: Set SYNC bit of TCR2 to Asynchronous Mode

commit 855675f6e6a65688a7f4cf45b9b5a98cf6c6f5c3 upstream.

There is one design rule according to SAI's reference manual:
If the transmitter bit clock and frame sync are to be used by both transmitter
and receiver, the transmitter must be configured for asynchronous operation
and the receiver for synchronous operation.

And SYNC of TCR2 is a 2-width control bit:
00 Asynchronous mode.
01 Synchronous with receiver.
10 Synchronous with another SAI transmitter.
11 Synchronous with another SAI receiver.

So the driver should have set SYNC bit of TCR2 to 0x0, and meanwhile set SYNC
bit of RCR2 to 0x1 (Synchronous with transmitter).

Signed-off-by: Nicolin Chen <nicoleotsuka@gmail.com>
Signed-off-by: Mark Brown <broonie@linaro.org>
Signed-off-by: Arnd Bergmann <arnd@arndb.de>
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 sound/soc/fsl/fsl_sai.c | 3 +--
 1 file changed, 1 insertion(+), 2 deletions(-)

--- a/sound/soc/fsl/fsl_sai.c
+++ b/sound/soc/fsl/fsl_sai.c
@@ -333,8 +333,7 @@ static int fsl_sai_trigger(struct snd_pc
 	 * The transmitter bit clock and frame sync are to be
 	 * used by both the transmitter and receiver.
 	 */
-	regmap_update_bits(sai->regmap, FSL_SAI_TCR2, FSL_SAI_CR2_SYNC,
-			   ~FSL_SAI_CR2_SYNC);
+	regmap_update_bits(sai->regmap, FSL_SAI_TCR2, FSL_SAI_CR2_SYNC, 0);
 	regmap_update_bits(sai->regmap, FSL_SAI_RCR2, FSL_SAI_CR2_SYNC,
 			   FSL_SAI_CR2_SYNC);
 
