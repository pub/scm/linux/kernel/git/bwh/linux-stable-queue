From: Arnd Bergmann <arnd@arndb.de>
Date: Fri,  5 May 2017 21:46:20 +0200
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Subject: Disable "frame-address" warning
Content-Transfer-Encoding: 8bit

From: Linus Torvalds <torvalds@linux-foundation.org>

commit 124a3d88fa20e1869fc229d7d8c740cc81944264 upstream.

Newer versions of gcc warn about the use of __builtin_return_address()
with a non-zero argument when "-Wall" is specified:

  kernel/trace/trace_irqsoff.c: In function ‘stop_critical_timings’:
  kernel/trace/trace_irqsoff.c:433:86: warning: calling ‘__builtin_return_address’ with a nonzero argument is unsafe [-Wframe-address]
     stop_critical_timing(CALLER_ADDR0, CALLER_ADDR1);
  [ .. repeats a few times for other similar cases .. ]

It is true that a non-zero argument is somewhat dangerous, and we do not
actually have very many uses of that in the kernel - but the ftrace code
does use it, and as Stephen Rostedt says:

 "We are well aware of the danger of using __builtin_return_address() of
  > 0.  In fact that's part of the reason for having the "thunk" code in
  x86 (See arch/x86/entry/thunk_{64,32}.S).  [..] it adds extra frames
  when tracking irqs off sections, to prevent __builtin_return_address()
  from accessing bad areas.  In fact the thunk_32.S states: 'Trampoline to
  trace irqs off.  (otherwise CALLER_ADDR1 might crash)'."

For now, __builtin_return_address() with a non-zero argument is the best
we can do, and the warning is not helpful and can end up making people
miss other warnings for real problems.

So disable the frame-address warning on compilers that need it.

Acked-by: Steven Rostedt <rostedt@goodmis.org>
Signed-off-by: Linus Torvalds <torvalds@linux-foundation.org>
Signed-off-by: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
Signed-off-by: Arnd Bergmann <arnd@arndb.de>
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 Makefile | 1 +
 1 file changed, 1 insertion(+)

--- a/Makefile
+++ b/Makefile
@@ -616,6 +616,7 @@ all: vmlinux
 include $(srctree)/arch/$(SRCARCH)/Makefile
 
 KBUILD_CFLAGS	+= $(call cc-option,-fno-delete-null-pointer-checks,)
+KBUILD_CFLAGS	+= $(call cc-disable-warning,frame-address,)
 
 ifdef CONFIG_CC_OPTIMIZE_FOR_SIZE
 KBUILD_CFLAGS	+= -Os $(call cc-disable-warning,maybe-uninitialized,)
