From: "Michael S. Tsirkin" <mst@redhat.com>
Date: Thu, 14 Jan 2016 16:00:41 +0200
Subject: virtio_pci: fix use after free on release

commit 2989be09a8a9d62a785137586ad941f916e08f83 upstream.

KASan detected a use-after-free error in virtio-pci remove code. In
virtio_pci_remove(), vp_dev is still used after being freed in
unregister_virtio_device() (in virtio_pci_release_dev() more
precisely).

To fix, keep a reference until cleanup is done.

Fixes: 63bd62a08ca4 ("virtio_pci: defer kfree until release callback")
Reported-by: Jerome Marchand <jmarchan@redhat.com>
Cc: Sasha Levin <sasha.levin@oracle.com>
Signed-off-by: Michael S. Tsirkin <mst@redhat.com>
Tested-by: Jerome Marchand <jmarchan@redhat.com>
[bwh: Backported to 3.2: adjust filename]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 drivers/virtio/virtio_pci.c | 2 ++
 1 file changed, 2 insertions(+)

--- a/drivers/virtio/virtio_pci.c
+++ b/drivers/virtio/virtio_pci.c
@@ -698,6 +698,7 @@ out:
 static void __devexit virtio_pci_remove(struct pci_dev *pci_dev)
 {
 	struct virtio_pci_device *vp_dev = pci_get_drvdata(pci_dev);
+	struct device *dev = get_device(&vp_dev->vdev.dev);
 
 	unregister_virtio_device(&vp_dev->vdev);
 
@@ -706,6 +707,7 @@ static void __devexit virtio_pci_remove(
 	pci_iounmap(pci_dev, vp_dev->ioaddr);
 	pci_release_regions(pci_dev);
 	pci_disable_device(pci_dev);
+	put_device(dev);
 }
 
 #ifdef CONFIG_PM
