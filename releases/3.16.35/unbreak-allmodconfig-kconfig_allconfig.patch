From: Al Viro <viro@ZenIV.linux.org.uk>
Date: Thu, 14 Jan 2016 18:13:49 +0000
Subject: unbreak allmodconfig KCONFIG_ALLCONFIG=...

commit 6b87b70c5339f30e3c5b32085e69625906513dc2 upstream.

	Prior to 3.13 make allmodconfig KCONFIG_ALLCONFIG=/dev/null used
to be equivalent to make allmodconfig; these days it hardwires MODULES to n.
In fact, any KCONFIG_ALLCONFIG that doesn't set MODULES explicitly is
treated as if it set it to n.

	Regression had been introduced by commit cfa98f ("kconfig: do not
override symbols already set"); what happens is that conf_read_simple()
does sym_calc_value(modules_sym) on exit, which leaves SYMBOL_VALID set and
has conf_set_all_new_symbols() skip modules_sym.

	It's pretty easy to fix - simply move that call of sym_calc_value()
into the callers, except for the ones in KCONFIG_ALLCONFIG handling.
Objections?

Signed-off-by: Al Viro <viro@zeniv.linux.org.uk>
Fixes: cfa98f2e0ae9 ("kconfig: do not override symbols already set")
Signed-off-by: Michal Marek <mmarek@suse.com>
[bwh: Backported to 3.16: old code also checked for modules_sym != NULL;
 drop the check since it's only useful for other projects]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
--- a/scripts/kconfig/confdata.c
+++ b/scripts/kconfig/confdata.c
@@ -261,11 +261,8 @@ int conf_read_simple(const char *name, i
 		if (in)
 			goto load;
 		sym_add_change_count(1);
-		if (!sym_defconfig_list) {
-			if (modules_sym)
-				sym_calc_value(modules_sym);
+		if (!sym_defconfig_list)
 			return 1;
-		}
 
 		for_all_defaults(sym_defconfig_list, prop) {
 			if (expr_calc_value(prop->visible.expr) == no ||
@@ -399,8 +396,6 @@ setsym:
 	free(line);
 	fclose(in);
 
-	if (modules_sym)
-		sym_calc_value(modules_sym);
 	return 0;
 }
 
@@ -411,8 +406,12 @@ int conf_read(const char *name)
 
 	sym_set_change_count(0);
 
-	if (conf_read_simple(name, S_DEF_USER))
+	if (conf_read_simple(name, S_DEF_USER)) {
+		sym_calc_value(modules_sym);
 		return 1;
+	}
+
+	sym_calc_value(modules_sym);
 
 	for_all_symbols(i, sym) {
 		sym_calc_value(sym);
@@ -843,6 +842,7 @@ static int conf_split_config(void)
 
 	name = conf_get_autoconfig_name();
 	conf_read_simple(name, S_DEF_AUTO);
+	sym_calc_value(modules_sym);
 
 	if (chdir("include/config"))
 		return 1;
