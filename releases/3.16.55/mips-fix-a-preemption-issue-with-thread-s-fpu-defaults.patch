From: "Maciej W. Rozycki" <macro@linux-mips.org>
Date: Tue, 12 May 2015 15:20:57 +0100
Subject: MIPS: Fix a preemption issue with thread's FPU defaults

commit 03dce595270f22d59a6f37e9170287c1afd94bc2 upstream.

Fix "BUG: using smp_processor_id() in preemptible" reported in accesses
to thread's FPU defaults: the value to initialise FSCR to at program
startup, the FCSR r/w mask and the contents of FIR in full FPU
emulation, removing a regression introduced with 9b26616c [MIPS: Respect
the ISA level in FCSR handling] and f6843626 [MIPS: math-emu: Set FIR
feature flags for full emulation].

Use `boot_cpu_data' to obtain the data from, following the approach that
`cpu_has_*' macros take and avoiding the call to `smp_processor_id' made
in the reference to `current_cpu_data'.  The contents of FSCR have to be
consistent across processors in an SMP system, the settings there must
not change as a thread is migrated across processors.  And the contents
of FIR are guaranteed to be consistent in FPU emulation, by definition.

Signed-off-by: Maciej W. Rozycki <macro@linux-mips.org>
Tested-by: Ezequiel Garcia <ezequiel.garcia@imgtec.com>
Tested-by: Paul Martin <paul.martin@codethink.co.uk>
Cc: Markos Chandras <Markos.Chandras@imgtec.com>
Cc: James Hogan <james.hogan@imgtec.com>
Cc: linux-mips@linux-mips.org
Patchwork: https://patchwork.linux-mips.org/patch/10030/
Signed-off-by: Ralf Baechle <ralf@linux-mips.org>
[bwh: Backported to 3.16:
 - Drop change in cop1_cfc()
 - Adjust context]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
--- a/arch/mips/include/asm/elf.h
+++ b/arch/mips/include/asm/elf.h
@@ -277,7 +277,7 @@ do {									\
 									\
 	current->thread.abi = &mips_abi;				\
 									\
-	current->thread.fpu.fcr31 = current_cpu_data.fpu_csr31;		\
+	current->thread.fpu.fcr31 = boot_cpu_data.fpu_csr31;		\
 } while (0)
 
 #endif /* CONFIG_32BIT */
@@ -337,7 +337,7 @@ do {									\
 	else								\
 		current->thread.abi = &mips_abi;			\
 									\
-	current->thread.fpu.fcr31 = current_cpu_data.fpu_csr31;		\
+	current->thread.fpu.fcr31 = boot_cpu_data.fpu_csr31;		\
 									\
 	p = personality(current->personality);				\
 	if (p != PER_LINUX32 && p != PER_LINUX)				\
--- a/arch/mips/kernel/ptrace.c
+++ b/arch/mips/kernel/ptrace.c
@@ -177,7 +177,7 @@ int ptrace_setfpregs(struct task_struct
 
 	__get_user(value, data + 64);
 	fcr31 = child->thread.fpu.fcr31;
-	mask = current_cpu_data.fpu_msk31;
+	mask = boot_cpu_data.fpu_msk31;
 	child->thread.fpu.fcr31 = (value & ~mask) | (fcr31 & mask);
 
 	/* FIR may not be written.  */
--- a/arch/mips/math-emu/cp1emu.c
+++ b/arch/mips/math-emu/cp1emu.c
@@ -934,7 +934,7 @@ emul:
 				 * Preserve read-only bits,
 				 * and convert to ieee library modes
 				 */
-				mask = current_cpu_data.fpu_msk31;
+				mask = boot_cpu_data.fpu_msk31;
 				ctx->fcr31 = (value & ~(mask | FPU_CSR_RM)) |
 					     (ctx->fcr31 & mask) |
 					     modeindex(value);
