From: Sean Young <sean@mess.org>
Date: Wed, 24 May 2017 06:24:51 -0300
Subject: [media] rc-core: race condition during ir_raw_event_register()

commit 963761a0b2e85663ee4a5630f72930885a06598a upstream.

A rc device can call ir_raw_event_handle() after rc_allocate_device(),
but before rc_register_device() has completed. This is racey because
rcdev->raw is set before rcdev->raw->thread has a valid value.

Reported-by: kbuild test robot <fengguang.wu@intel.com>
Signed-off-by: Sean Young <sean@mess.org>
Signed-off-by: Mauro Carvalho Chehab <mchehab@s-opensource.com>
[bwh: Backported to 3.16: adjust filename, context, indentation]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 drivers/media/rc/ir-raw.c | 13 ++++++++-----
 1 file changed, 8 insertions(+), 5 deletions(-)

--- a/drivers/media/rc/ir-raw.c
+++ b/drivers/media/rc/ir-raw.c
@@ -220,7 +220,7 @@ void ir_raw_event_handle(struct rc_dev *
 {
 	unsigned long flags;
 
-	if (!dev->raw)
+	if (!dev->raw || !dev->raw->thread)
 		return;
 
 	spin_lock_irqsave(&dev->raw->lock, flags);
@@ -247,6 +247,7 @@ int ir_raw_event_register(struct rc_dev
 {
 	int rc;
 	struct ir_raw_handler *handler;
+	struct task_struct *thread;
 
 	if (!dev)
 		return -EINVAL;
@@ -264,14 +265,16 @@ int ir_raw_event_register(struct rc_dev
 		goto out;
 
 	spin_lock_init(&dev->raw->lock);
-	dev->raw->thread = kthread_run(ir_raw_event_thread, dev->raw,
-				       "rc%ld", dev->devno);
+	thread = kthread_run(ir_raw_event_thread, dev->raw, "rc%ld",
+			     dev->devno);
 
-	if (IS_ERR(dev->raw->thread)) {
-		rc = PTR_ERR(dev->raw->thread);
+	if (IS_ERR(thread)) {
+		rc = PTR_ERR(thread);
 		goto out;
 	}
 
+	dev->raw->thread = thread;
+
 	mutex_lock(&ir_raw_handler_lock);
 	list_add_tail(&dev->raw->list, &ir_raw_client_list);
 	list_for_each_entry(handler, &ir_raw_handler_list, list)
