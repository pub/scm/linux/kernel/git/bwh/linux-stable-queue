From: "Paul E. McKenney" <paulmck@linux.vnet.ibm.com>
Date: Tue, 1 Sep 2015 00:42:57 -0700
Subject: rcu: Move preemption disabling out of __srcu_read_lock()

commit 49f5903b473c5f63f3b57856d1bd4593db0a2eef upstream.

Currently, __srcu_read_lock() cannot be invoked from restricted
environments because it contains calls to preempt_disable() and
preempt_enable(), both of which can invoke lockdep, which is a bad
idea in some restricted execution modes.  This commit therefore moves
the preempt_disable() and preempt_enable() from __srcu_read_lock()
to srcu_read_lock().  It also inserts the preempt_disable() and
preempt_enable() around the call to __srcu_read_lock() in do_exit().

Signed-off-by: Paul E. McKenney <paulmck@linux.vnet.ibm.com>
Reviewed-by: Josh Triplett <josh@joshtriplett.org>
[bwh: Backported to 3.16:
 - Drop changes in do_exit()
 - Adjust context]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
--- a/include/linux/srcu.h
+++ b/include/linux/srcu.h
@@ -217,8 +217,11 @@ static inline int srcu_read_lock_held(st
  */
 static inline int srcu_read_lock(struct srcu_struct *sp) __acquires(sp)
 {
-	int retval = __srcu_read_lock(sp);
+	int retval;
 
+	preempt_disable();
+	retval = __srcu_read_lock(sp);
+	preempt_enable();
 	rcu_lock_acquire(&(sp)->dep_map);
 	return retval;
 }
--- a/kernel/rcu/srcu.c
+++ b/kernel/rcu/srcu.c
@@ -297,11 +297,9 @@ int __srcu_read_lock(struct srcu_struct
 	int idx;
 
 	idx = ACCESS_ONCE(sp->completed) & 0x1;
-	preempt_disable();
 	ACCESS_ONCE(this_cpu_ptr(sp->per_cpu_ref)->c[idx]) += 1;
 	smp_mb(); /* B */  /* Avoid leaking the critical section. */
 	ACCESS_ONCE(this_cpu_ptr(sp->per_cpu_ref)->seq[idx]) += 1;
-	preempt_enable();
 	return idx;
 }
 EXPORT_SYMBOL_GPL(__srcu_read_lock);
