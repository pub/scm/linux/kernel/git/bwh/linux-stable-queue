From: H Hartley Sweeten <hsweeten@visionengravers.com>
Date: Mon, 5 Oct 2015 14:22:52 -0700
Subject: staging: comedi: quatech_daqp_cs: use comedi_timeout() in ao (*insn_write)

commit e031642eccc040648b09cfc7d632e2e8d0b6f94f upstream.

The data link between the D/A data port and the D/A converter is a serial
link. The serial link requires about 8ms to complete a transfer. Use the
comedi_timeout() helper to ensure that there is not a previous transfer
still happening before trying to write new data to the channel.

Signed-off-by: H Hartley Sweeten <hsweeten@visionengravers.com>
Reviewed-by: Ian Abbott <abbotti@mev.co.uk>
Signed-off-by: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
[ija: Backported to 3.16: No 'readback' member in subdevice.]
Signed-off-by: Ian Abbott <abbotti@mev.co.uk>
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 .../staging/comedi/drivers/quatech_daqp_cs.c  | 19 +++++++++++++++++++
 1 file changed, 19 insertions(+)

--- a/drivers/staging/comedi/drivers/quatech_daqp_cs.c
+++ b/drivers/staging/comedi/drivers/quatech_daqp_cs.c
@@ -633,6 +633,19 @@ static int daqp_ai_cmd(struct comedi_dev
 	return 0;
 }
 
+static int daqp_ao_empty(struct comedi_device *dev,
+			 struct comedi_subdevice *s,
+			 struct comedi_insn *insn,
+			 unsigned long context)
+{
+	unsigned int status;
+
+	status = inb(dev->iobase + DAQP_AUX);
+	if ((status & DAQP_AUX_DA_BUFFER) == 0)
+		return 0;
+	return -EBUSY;
+}
+
 static int daqp_ao_insn_write(struct comedi_device *dev,
 			      struct comedi_subdevice *s,
 			      struct comedi_insn *insn,
@@ -650,6 +663,12 @@ static int daqp_ao_insn_write(struct com
 
 	for (i = 0; i > insn->n; i++) {
 		unsigned val = data[i];
+		int ret;
+
+		/* D/A transfer rate is about 8ms */
+		ret = comedi_timeout(dev, s, insn, daqp_ao_empty, 0);
+		if (ret)
+			return ret;
 
 		val &= 0x0fff;
 		val ^= 0x0800;		/* Flip the sign */
