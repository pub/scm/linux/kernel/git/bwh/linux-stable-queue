From: Takashi Iwai <tiwai@suse.de>
Date: Fri, 9 Nov 2018 11:59:45 +0100
Subject: ALSA: oss: Use kvzalloc() for local buffer allocations

commit 65766ee0bf7fe8b3be80e2e1c3ef54ad59b29476 upstream.

PCM OSS layer may allocate a few temporary buffers, one for the core
read/write and another for the conversions via plugins.  Currently
both are allocated via vmalloc().  But as the allocation size is
equivalent with the PCM period size, the required size might be quite
small, depending on the application.

This patch replaces these vmalloc() calls with kvzalloc() for covering
small period sizes better.  Also, we use "z"-alloc variant here for
addressing the possible uninitialized access reported by syzkaller.

Reported-by: syzbot+1cb36954e127c98dd037@syzkaller.appspotmail.com
Signed-off-by: Takashi Iwai <tiwai@suse.de>
[bwh: Backported to 3.16: kvzalloc() does not exist, so only change to
 using vzalloc()]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
--- a/sound/core/oss/pcm_oss.c
+++ b/sound/core/oss/pcm_oss.c
@@ -1075,7 +1075,7 @@ static int snd_pcm_oss_change_params_loc
 	runtime->oss.rate = params_rate(params);
 
 	vfree(runtime->oss.buffer);
-	runtime->oss.buffer = vmalloc(runtime->oss.period_bytes);
+	runtime->oss.buffer = vzalloc(runtime->oss.period_bytes);
 	if (!runtime->oss.buffer) {
 		err = -ENOMEM;
 		goto failure;
--- a/sound/core/oss/pcm_plugin.c
+++ b/sound/core/oss/pcm_plugin.c
@@ -67,7 +67,7 @@ static int snd_pcm_plugin_alloc(struct s
 	size /= 8;
 	if (plugin->buf_frames < frames) {
 		vfree(plugin->buf);
-		plugin->buf = vmalloc(size);
+		plugin->buf = vzalloc(size);
 		plugin->buf_frames = frames;
 	}
 	if (!plugin->buf) {
