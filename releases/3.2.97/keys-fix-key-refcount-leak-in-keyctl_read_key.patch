From: Eric Biggers <ebiggers@google.com>
Date: Mon, 18 Sep 2017 11:36:31 -0700
Subject: KEYS: fix key refcount leak in keyctl_read_key()

commit 7fc0786d956d9e59b68d282be9b156179846ea3d upstream.

In keyctl_read_key(), if key_permission() were to return an error code
other than EACCES, we would leak a the reference to the key.  This can't
actually happen currently because key_permission() can only return an
error code other than EACCES if security_key_permission() does, only
SELinux and Smack implement that hook, and neither can return an error
code other than EACCES.  But it should still be fixed, as it is a bug
waiting to happen.

Fixes: 29db91906340 ("[PATCH] Keys: Add LSM hooks for key management [try #3]")
Signed-off-by: Eric Biggers <ebiggers@google.com>
Signed-off-by: David Howells <dhowells@redhat.com>
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 security/keys/keyctl.c | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

--- a/security/keys/keyctl.c
+++ b/security/keys/keyctl.c
@@ -684,7 +684,7 @@ long keyctl_read_key(key_serial_t keyid,
 	if (ret == 0)
 		goto can_read_key;
 	if (ret != -EACCES)
-		goto error;
+		goto error2;
 
 	/* we can't; see if it's searchable from this process's keyrings
 	 * - we automatically take account of the fact that it may be
