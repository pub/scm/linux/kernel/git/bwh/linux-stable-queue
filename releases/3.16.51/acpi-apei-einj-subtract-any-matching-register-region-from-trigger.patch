From: Yazen Ghannam <yazen.ghannam@amd.com>
Date: Mon, 28 Aug 2017 13:13:14 -0500
Subject: ACPI, APEI, EINJ: Subtract any matching Register Region from Trigger
 resources

commit 1d5d820b8fe83b5f859d1ebb028a09ada426447e upstream.

ACPI defines a number of instructions to use for triggering errors. However
we are currently removing the address resources from the trigger resources
for only the WRITE_REGISTER_VALUE instruction. This leads to a resource
conflict for any other valid instruction.

Check that the instruction is less than or equal to the
WRITE_REGISTER_VALUE instruction. This allows all valid memory access
instructions and protects against invalid instructions.

Fixes: b4e008dc53a3 (ACPI, APEI, EINJ, Refine the fix of resource conflict)
Signed-off-by: Yazen Ghannam <yazen.ghannam@amd.com>
Acked-by: Tony Luck <tony.luck@intel.com>
Signed-off-by: Rafael J. Wysocki <rafael.j.wysocki@intel.com>
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 drivers/acpi/apei/einj.c | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

--- a/drivers/acpi/apei/einj.c
+++ b/drivers/acpi/apei/einj.c
@@ -285,7 +285,7 @@ static struct acpi_generic_address *einj
 		((char *)trigger_tab + sizeof(struct acpi_einj_trigger));
 	for (i = 0; i < trigger_tab->entry_count; i++) {
 		if (entry->action == ACPI_EINJ_TRIGGER_ERROR &&
-		entry->instruction == ACPI_EINJ_WRITE_REGISTER_VALUE &&
+		entry->instruction <= ACPI_EINJ_WRITE_REGISTER_VALUE &&
 		entry->register_region.space_id ==
 			ACPI_ADR_SPACE_SYSTEM_MEMORY &&
 		(entry->register_region.address & param2) == (param1 & param2))
