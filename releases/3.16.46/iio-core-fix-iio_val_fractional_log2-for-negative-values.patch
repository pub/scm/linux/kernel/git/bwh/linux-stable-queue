From: Nikolaus Schulz <nikolaus.schulz@avionic-design.de>
Date: Fri, 24 Mar 2017 13:41:51 +0100
Subject: iio: core: Fix IIO_VAL_FRACTIONAL_LOG2 for negative values

commit 7fd6592d1287046f61bfd3cda3c03cd35be490f7 upstream.

Fix formatting of negative values of type IIO_VAL_FRACTIONAL_LOG2 by
switching from do_div(), which can't handle negative numbers, to
div_s64_rem().  Also use shift_right for shifting, which is safe with
negative values.

Signed-off-by: Nikolaus Schulz <nikolaus.schulz@avionic-design.de>
Reviewed-by: Lars-Peter Clausen <lars@metafoo.de>
Signed-off-by: Jonathan Cameron <jic23@kernel.org>
[bwh: Backported to 3.16:
 - Use vals[] instead of tmp{0,1}
 - Keep using sprintf()]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 drivers/iio/industrialio-core.c | 7 +++----
 1 file changed, 3 insertions(+), 4 deletions(-)

--- a/drivers/iio/industrialio-core.c
+++ b/drivers/iio/industrialio-core.c
@@ -406,10 +406,9 @@ ssize_t iio_format_value(char *buf, unsi
 		vals[0] = (int)div_s64_rem(tmp, 1000000000, &vals[1]);
 		return sprintf(buf, "%d.%09u\n", vals[0], abs(vals[1]));
 	case IIO_VAL_FRACTIONAL_LOG2:
-		tmp = (s64)vals[0] * 1000000000LL >> vals[1];
-		vals[1] = do_div(tmp, 1000000000LL);
-		vals[0] = tmp;
-		return sprintf(buf, "%d.%09u\n", vals[0], vals[1]);
+		tmp = shift_right((s64)vals[0] * 1000000000LL, vals[1]);
+		vals[0] = (int)div_s64_rem(tmp, 1000000000LL, &vals[1]);
+		return sprintf(buf, "%d.%09u\n", vals[0], abs(vals[1]));
 	case IIO_VAL_INT_MULTIPLE:
 	{
 		int i;
