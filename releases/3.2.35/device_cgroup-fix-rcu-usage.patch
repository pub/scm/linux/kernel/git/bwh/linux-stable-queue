From: Tejun Heo <tj@kernel.org>
Date: Tue, 6 Nov 2012 09:17:37 -0800
Subject: device_cgroup: fix RCU usage

commit 201e72acb2d3821e2de9ce6091e98859c316b29a upstream.

dev_cgroup->exceptions is protected with devcgroup_mutex for writes
and RCU for reads; however, RCU usage isn't correct.

* dev_exception_clean() doesn't use RCU variant of list_del() and
  kfree().  The function can race with may_access() and may_access()
  may end up dereferencing already freed memory.  Use list_del_rcu()
  and kfree_rcu() instead.

* may_access() may be called only with RCU read locked but doesn't use
  RCU safe traversal over ->exceptions.  Use list_for_each_entry_rcu().

Signed-off-by: Tejun Heo <tj@kernel.org>
Acked-by: Serge E. Hallyn <serge.hallyn@ubuntu.com>
Cc: Aristeu Rozanski <aris@redhat.com>
Cc: Li Zefan <lizefan@huawei.com>
[bwh: Backported to 3.2:
 - Adjust context
 - Exception list is called whitelist]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 security/device_cgroup.c |    6 +++---
 1 file changed, 3 insertions(+), 3 deletions(-)

--- a/security/device_cgroup.c
+++ b/security/device_cgroup.c
@@ -202,8 +202,8 @@ static void devcgroup_destroy(struct cgr
 
 	dev_cgroup = cgroup_to_devcgroup(cgroup);
 	list_for_each_entry_safe(wh, tmp, &dev_cgroup->whitelist, list) {
-		list_del(&wh->list);
-		kfree(wh);
+		list_del_rcu(&wh->list);
+		kfree_rcu(wh, rcu);
 	}
 	kfree(dev_cgroup);
 }
@@ -278,7 +278,7 @@ static int may_access_whitelist(struct d
 {
 	struct dev_whitelist_item *whitem;
 
-	list_for_each_entry(whitem, &c->whitelist, list) {
+	list_for_each_entry_rcu(whitem, &c->whitelist, list) {
 		if (whitem->type & DEV_ALL)
 			return 1;
 		if ((refwh->type & DEV_BLOCK) && !(whitem->type & DEV_BLOCK))
