From: Todd E Brandt <todd.e.brandt@linux.intel.com>
Date: Wed, 2 Mar 2016 16:05:29 -0800
Subject: PM / sleep / x86: Fix crash on graph trace through x86 suspend

commit 92f9e179a702a6adbc11e2fedc76ecd6ffc9e3f7 upstream.

Pause/unpause graph tracing around do_suspend_lowlevel as it has
inconsistent call/return info after it jumps to the wakeup vector.
The graph trace buffer will otherwise become misaligned and
may eventually crash and hang on suspend.

To reproduce the issue and test the fix:
Run a function_graph trace over suspend/resume and set the graph
function to suspend_devices_and_enter. This consistently hangs the
system without this fix.

Signed-off-by: Todd Brandt <todd.e.brandt@linux.intel.com>
Signed-off-by: Rafael J. Wysocki <rafael.j.wysocki@intel.com>
[bwh: Backported to 3.2: adjust context]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 arch/x86/kernel/acpi/sleep.c | 7 +++++++
 1 file changed, 7 insertions(+)

--- a/arch/x86/kernel/acpi/sleep.c
+++ b/arch/x86/kernel/acpi/sleep.c
@@ -15,6 +15,7 @@
 #include <asm/pgtable.h>
 #include <asm/cacheflush.h>
 
+#include <linux/ftrace.h>
 #include "realmode/wakeup.h"
 #include "sleep.h"
 
@@ -102,7 +103,13 @@ int acpi_suspend_lowlevel(void)
        saved_magic = 0x123456789abcdef0L;
 #endif /* CONFIG_64BIT */
 
+	/*
+	 * Pause/unpause graph tracing around do_suspend_lowlevel as it has
+	 * inconsistent call/return info after it jumps to the wakeup vector.
+	 */
+	pause_graph_tracing();
 	do_suspend_lowlevel();
+	unpause_graph_tracing();
 	return 0;
 }
 
