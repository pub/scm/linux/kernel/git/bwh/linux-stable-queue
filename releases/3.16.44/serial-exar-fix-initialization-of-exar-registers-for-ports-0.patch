From: Jan Kiszka <jan.kiszka@siemens.com>
Date: Wed, 8 Feb 2017 17:09:04 +0100
Subject: serial: exar: Fix initialization of EXAR registers for ports > 0

commit 5b5f252d67afd7bd5b923c664206d60800bf5054 upstream.

So far, pci_xr17v35x_setup always initialized 8XMODE, FCTR & Co. for
port 0 because it used the address of that port instead of moving the
pointer according to the port number. Fix this and remove the unneeded
temporary ioremap by moving default_setup up and reusing the membase it
fills into the port structure.

Fixes: 14faa8cce88e ("tty/8250 Add support for Commtech's Fastcom Async-335 and Fastcom Async-PCIe cards")
Signed-off-by: Jan Kiszka <jan.kiszka@siemens.com>
Reviewed-by: Andy Shevchenko <andy.shevchenko@gmail.com>
Signed-off-by: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
[bwh: Backported to 3.16:
 - Adjust filename, context
 - Add 'ret' variable]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
--- a/drivers/tty/serial/8250/8250_pci.c
+++ b/drivers/tty/serial/8250/8250_pci.c
@@ -1661,10 +1661,7 @@ pci_xr17v35x_setup(struct serial_private
 		  struct uart_8250_port *port, int idx)
 {
 	u8 __iomem *p;
-
-	p = pci_ioremap_bar(priv->dev, 0);
-	if (p == NULL)
-		return -ENOMEM;
+	int ret;
 
 	port->port.flags |= UPF_EXAR_EFR;
 
@@ -1675,6 +1672,17 @@ pci_xr17v35x_setup(struct serial_private
 	if (xr17v35x_has_slave(priv) && idx >= 8)
 		port->port.uartclk = (7812500 * 16 / 2);
 
+	ret = pci_default_setup(priv, board, port, idx);
+	if (ret)
+		return ret;
+
+	p = port->port.membase;
+
+	writeb(0x00, p + UART_EXAR_8XMODE);
+	writeb(UART_FCTR_EXAR_TRGD, p + UART_EXAR_FCTR);
+	writeb(128, p + UART_EXAR_TXTRG);
+	writeb(128, p + UART_EXAR_RXTRG);
+
 	/*
 	 * Setup Multipurpose Input/Output pins.
 	 */
@@ -1692,13 +1700,8 @@ pci_xr17v35x_setup(struct serial_private
 		writeb(0x00, p + 0x99); /*MPIOSEL[15:8]*/
 		writeb(0x00, p + 0x9a); /*MPIOOD[15:8]*/
 	}
-	writeb(0x00, p + UART_EXAR_8XMODE);
-	writeb(UART_FCTR_EXAR_TRGD, p + UART_EXAR_FCTR);
-	writeb(128, p + UART_EXAR_TXTRG);
-	writeb(128, p + UART_EXAR_RXTRG);
-	iounmap(p);
 
-	return pci_default_setup(priv, board, port, idx);
+	return 0;
 }
 
 #define PCI_DEVICE_ID_COMMTECH_4222PCI335 0x0004
