From: Jan Kara <jack@suse.cz>
Date: Tue, 13 Jun 2017 15:54:58 +0200
Subject: udf: Fix races with i_size changes during readpage

commit 9795e0e8ac0d6a3ee092f1b555b284b57feef99e upstream.

__udf_adinicb_readpage() uses i_size several times. When truncate
changes i_size while the function is running, it can observe several
different values and thus e.g. expose uninitialized parts of page to
userspace. Also use i_size_read() in the function since it does not hold
inode_lock. Since i_size is guaranteed to be small, this cannot really
cause any issues even on 32-bit archs but let's be careful.

Fixes: 9c2fc0de1a6e638fe58c354a463f544f42a90a09
Signed-off-by: Jan Kara <jack@suse.cz>
[bwh: Backported to 3.2: adjust context]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 fs/udf/file.c | 9 +++++++--
 1 file changed, 7 insertions(+), 2 deletions(-)

--- a/fs/udf/file.c
+++ b/fs/udf/file.c
@@ -44,10 +44,15 @@ static void __udf_adinicb_readpage(struc
 	struct inode *inode = page->mapping->host;
 	char *kaddr;
 	struct udf_inode_info *iinfo = UDF_I(inode);
+	loff_t isize = i_size_read(inode);
 
+	/*
+	 * We have to be careful here as truncate can change i_size under us.
+	 * So just sample it once and use the same value everywhere.
+	 */
 	kaddr = kmap(page);
-	memcpy(kaddr, iinfo->i_ext.i_data + iinfo->i_lenEAttr, inode->i_size);
-	memset(kaddr + inode->i_size, 0, PAGE_CACHE_SIZE - inode->i_size);
+	memcpy(kaddr, iinfo->i_ext.i_data + iinfo->i_lenEAttr, isize);
+	memset(kaddr + isize, 0, PAGE_CACHE_SIZE - isize);
 	flush_dcache_page(page);
 	SetPageUptodate(page);
 	kunmap(page);
