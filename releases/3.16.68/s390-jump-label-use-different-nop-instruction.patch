From: Heiko Carstens <heiko.carstens@de.ibm.com>
Date: Thu, 29 Jan 2015 14:10:22 +0100
Subject: s390/jump label: use different nop instruction

commit d5caa4dbf9bd2ad8cd7f6be0ca76722be947182b upstream.

Use a brcl 0,2 instruction for jump label nops during compile time,
so we don't mix up the different nops during mcount/hotpatch call
site detection.
The initial jump label code instruction replacement will exchange
these instructions with either a branch or a brcl 0,0 instruction.

Signed-off-by: Heiko Carstens <heiko.carstens@de.ibm.com>
Signed-off-by: Martin Schwidefsky <schwidefsky@de.ibm.com>
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 arch/s390/include/asm/jump_label.h |  7 ++++++-
 arch/s390/kernel/jump_label.c      | 19 +++++++++++++------
 2 files changed, 19 insertions(+), 7 deletions(-)

--- a/arch/s390/include/asm/jump_label.h
+++ b/arch/s390/include/asm/jump_label.h
@@ -4,6 +4,7 @@
 #include <linux/types.h>
 
 #define JUMP_LABEL_NOP_SIZE 6
+#define JUMP_LABEL_NOP_OFFSET 2
 
 #ifdef CONFIG_64BIT
 #define ASM_PTR ".quad"
@@ -13,9 +14,13 @@
 #define ASM_ALIGN ".balign 4"
 #endif
 
+/*
+ * We use a brcl 0,2 instruction for jump labels at compile time so it
+ * can be easily distinguished from a hotpatch generated instruction.
+ */
 static __always_inline bool arch_static_branch(struct static_key *key)
 {
-	asm_volatile_goto("0:	brcl 0,0\n"
+	asm_volatile_goto("0:	brcl 0,"__stringify(JUMP_LABEL_NOP_OFFSET)"\n"
 		".pushsection __jump_table, \"aw\"\n"
 		ASM_ALIGN "\n"
 		ASM_PTR " 0b, %l[label], %0\n"
--- a/arch/s390/kernel/jump_label.c
+++ b/arch/s390/kernel/jump_label.c
@@ -49,6 +49,11 @@ static void jump_label_bug(struct jump_e
 	panic("Corrupted kernel text");
 }
 
+static struct insn orignop = {
+	.opcode = 0xc004,
+	.offset = JUMP_LABEL_NOP_OFFSET >> 1,
+};
+
 static void __jump_label_transform(struct jump_entry *entry,
 				   enum jump_label_type type,
 				   int init)
@@ -59,14 +64,16 @@ static void __jump_label_transform(struc
 		jump_label_make_nop(entry, &old);
 		jump_label_make_branch(entry, &new);
 	} else {
-		if (init)
-			jump_label_make_nop(entry, &old);
-		else
-			jump_label_make_branch(entry, &old);
+		jump_label_make_branch(entry, &old);
 		jump_label_make_nop(entry, &new);
 	}
-	if (memcmp((void *)entry->code, &old, sizeof(old)))
-		jump_label_bug(entry, &old);
+	if (init) {
+		if (memcmp((void *)entry->code, &orignop, sizeof(orignop)))
+			jump_label_bug(entry, &old);
+	} else {
+		if (memcmp((void *)entry->code, &old, sizeof(old)))
+			jump_label_bug(entry, &old);
+	}
 	probe_kernel_write((void *)entry->code, &new, sizeof(new));
 }
 
