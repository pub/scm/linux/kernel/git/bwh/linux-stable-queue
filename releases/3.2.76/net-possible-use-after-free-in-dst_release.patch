From: Francesco Ruggeri <fruggeri@aristanetworks.com>
Date: Wed, 6 Jan 2016 00:18:48 -0800
Subject: net: possible use after free in dst_release

commit 07a5d38453599052aff0877b16bb9c1585f08609 upstream.

dst_release should not access dst->flags after decrementing
__refcnt to 0. The dst_entry may be in dst_busy_list and
dst_gc_task may dst_destroy it before dst_release gets a chance
to access dst->flags.

Fixes: d69bbf88c8d0 ("net: fix a race in dst_release()")
Fixes: 27b75c95f10d ("net: avoid RCU for NOCACHE dst")
Signed-off-by: Francesco Ruggeri <fruggeri@arista.com>
Acked-by: Eric Dumazet <edumazet@google.com>
Signed-off-by: David S. Miller <davem@davemloft.net>
[bwh: Backported to 3.2: adjust context]
Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
 net/core/dst.c | 3 ++-
 1 file changed, 2 insertions(+), 1 deletion(-)

--- a/net/core/dst.c
+++ b/net/core/dst.c
@@ -269,10 +269,11 @@ void dst_release(struct dst_entry *dst)
 {
 	if (dst) {
 		int newrefcnt;
+		unsigned short nocache = dst->flags & DST_NOCACHE;
 
 		newrefcnt = atomic_dec_return(&dst->__refcnt);
 		WARN_ON(newrefcnt < 0);
-		if (!newrefcnt && unlikely(dst->flags & DST_NOCACHE)) {
+		if (!newrefcnt && unlikely(nocache)) {
 			dst = dst_destroy(dst);
 			if (dst)
 				__dst_free(dst);
